//
//  TrayConstants.h
//  TrayCommerce
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#ifndef TrayCommerce_TrayConstants_h
#define TrayCommerce_TrayConstants_h


/**
 * BASE URL API
 **/

#define TRAY_CHECKOUT_BASE_URL @"https://api.traycheckout.com.br/v2/"
#define TRAY_CHECKOUT_BASE_URL_CURRENT @"https://api.traycheckout.com.br/edge/"

#define MOBFIRST_HEADER_AUTHORIZATION @"Authorization"

#define MOBFIRST_REFRESH_BASE_URL @"http://apps.mobfirst.com/traycommerce"


/*
 * AUTH STRING MOBFIRST
 */

#define HEADER_MOBFIRST_CLIENT_APP_ID_VALUE @"2061"


#define HEADER_MOBFIRST_CLIENT_ID @"MobfirstClientId"
#define HEADER_MOBFIRST_CLIENT_ID_VALUE @"traycommerce"
#define HEADER_MOBFIRST_CLIENT_SECRET @"MobfirstClientSecret"
#define HEADER_MOBFIRST_CLIENT_APP_ID @"MobfirstAppId"
#define HEADER_MOBFIRST_CLIENT_STORE_CODE @"TrayCommerceStoreCode"


#define HEADER_MOBFIRST_CLIENT_STORE_CODE @"TrayCommerceStoreCode"
#define MOBFIRST_HEADER_CLIENT_SECRET_VALUE @"HKwUI7SHmB-LQe95EKok"

#define HEADER_CONSUMER_KEY @"consumer_key"
#define HEADER_CONSUMER_SECRET @"consumer_secret"
#define HEADER_CONSUMER_CODE @"code"

/**
 *HEADERS STRING
 **/

#define HEADER_TOKEN @"access_token"
#define HEADER_REFRESH_TOKEN @"refresh_token"

#define REGISTRY_PARAM_NAME @"registry"
#define REGISTRY_PARAM_VALUE @"rg_obrigatorio,cpf,cnpj,sem_estoque,sem_imagem,sem_preco,sem_peso,preco_prod_indisponivel"



#define HEADER_ATTTRS @"attrs"
#define HEADER_LIMIT @"limit"
#define HEADER_PAGE @"page"
#define HEADER_AVAILABLE @"available"
#define HEADER_AVAILABLE_VALUE @"1"
#define HEADER_SORT @"sort"
#define HEADER_NAME @"query"
#define HEADER_CATEGORY_ID @"category_id"
#define HEADER_ID @"id"
#define HEADER_CUSTOMER_ID @"customer_id"
#define HEADER_SESSION_ID @"session_id"
#define HEADER_PRODUCT_ID @"product_id"
#define HEADER_SORT_ASC @"_asc"
#define HEADER_SORT_DESC @"_desc"

#define HEADER_STATUS @"status"
#define HEADER_ACTIVE @"1"
#define HEADER_ZIP_CODE @"zipcode"

#define HEADER_PRODUCTS @"products"
#define HEADER_ARRAY_OPEN @"%5B"
#define HEADER_ARRAY_CLOSE @"%5D"
#define HEADER_PRICE @"price"
#define HEADER_QUANTITY @"quantity"

#define HEADER_RESELLER_TOKEN @"reseller_token"
#define HEADER_RESELLER_TOKEN_VALUE @"e9h9hdas8asdb11"

/**
 * HTTP STATUS SUPPORT CODE
 * */

#define HTTP_RESPONSE_SUCESS_200 200
#define HTTP_RESPONSE_SUCESS_201 201
#define HTTP_BAD_REQUEST_400 400
#define HTTP_UNAUTHORIZED_401 401
#define HTTP_NOT_FOUND_404 404

/**
 *  ERROR
 * */

#define CONVERT_ERROR_GENERIC @"Error on conversion"


/**
 * STORAGE USER
 * */

#define STORAGE_USER_ID @ "STORAGE_USER_ID"
#define STORAGE_USER_NAME @ "STORAGE_USER_NAME"
#define STORAGE_USER_EMAIL @ "STORAGE_USER_EMAIL"
#define STORAGE_USER_TOKEN @ "STORAGE_USER_TOKEN"
#define STORAGE_USER_CPF @ "STORAGE_USER_CPF"
#define STORAGE_USER_RG @ "STORAGE_USER_RG"
#define STORAGE_USER_PHONE @ "STORAGE_USER_PHONE"
#define STORAGE_USER_ADDRESSES_ID_SIZE @ "STORAGE_USER_ADDRESSES_ID_SIZE"
#define STORAGE_USER_ADDRESSES_ITEM @ "STORAGE_USER_ADDRESSES_ITEM_"


/**
 * JSON OBJECT KEY
 * */


/**
 * USERS
 * */
#define CUSTOMER @ "Customer"
#define USER_NAME @ "name"
#define USER_EMAIL @ "email"
#define USER_CPF @ "cpf"
#define USER_PHONE @ "phone"
#define USER_PASSWORD @ "password"
#define USER_RG @ "rg"


/**
 * CART
 * */

#define CART_CART @ "Cart"
#define CART_SESSION_ID @ "session_id"
#define CART_PRODUCT_ID @ "product_id"
#define CART_QUANTITY @ "quantity"
#define CART_PRICE @ "price"
#define CART_VARIANT_ID @ "variant_id"


/**
 * ADDRESSES
 * */

#define ADDRESSES_ID @ "id"
#define ADDRESSES_CUSTOMER_ID @ "customer_id"
#define ADDRESSES_LINE_1 @ "address"
#define ADDRESSES_LINE_2 @ "complement"
#define ADDRESSES_NUMBER @ "number"
#define ADDRESSES_NEIGHBORHOOD @ "neighborhood"
#define ADDRESSES_CITY @ "city"
#define ADDRESSES_STATE @ "state"
#define ADDRESSES_POSTAL_CODE @ "zip_code"
#define ADDRESSES_COUNTRY @ "country"
#define ADDRESSES_COUNTRY_CODE @ "BRA"


/**
 * ORDER
 * */

#define ORDER @ "Order"
#define ORDER_POINT_SALE @ "point_sale"
#define ORDER_POINT_SALE_APP @ "APP"
#define ORDER_SHIPPING @ "shipment"
#define ORDER_SHIPPING_VALUE @ "shipment_value"
#define ORDER_PAYMENT_FORM @ "payment_form"
#define ORDER_PAYMENT_METHOD_ID @ "payment_method_id"
#define ORDER_CUSTOMER @ "Customer"
#define ORDER_CUSTOMER_NAME @ "name"
#define ORDER_CUSTOMER_CPF @ "cpf"
#define ORDER_CUSTOMER_EMAIL @ "email"
#define ORDER_CUSTOMER_PHONE @ "phone"

#define ORDER_CUSTOMER_ADDRESS @ "CustomerAddress"

#define ORDER_PRODUCT_SOLD @ "ProductsSold"
#define ORDER_PRODUCT_SOLD_PRODUCT_ID @ "product_id"
#define ORDER_PRODUCT_SOLD_QUANTITY @ "quantity"
#define ORDER_PRODUCT_SOLD_PRICE @ "price"
#define ORDER_PRODUCT_SOLD_VARIANT_ID @ "variant_id"
#define ORDER_PRODUCT_SOLD_ORIGINAL_PRICE @ "original_price"

#endif
