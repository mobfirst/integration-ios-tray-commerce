//
//  ServiceStorageUser.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/12/14.
//  Copyright (c) 2014 Vitrina. All rights reserved.
//


#import "ServiceStorageUser.h"

@implementation ServiceStorageUser

+(BOOL) userIsLogged{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:STORAGE_USER_ID] != nil;
}

+(BOOL) saveUser:(User*)object{
    
    TrayUser* user = (TrayUser*) object;
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:user.id forKey:STORAGE_USER_ID];
    [defaults setObject:user.name forKey:STORAGE_USER_NAME];
    [defaults setObject:user.email forKey:STORAGE_USER_EMAIL];
    [defaults setObject:user.token forKey:STORAGE_USER_TOKEN];
    [defaults setObject:user.personalID forKey:STORAGE_USER_CPF];
    [defaults setObject:user.personalRg forKey:STORAGE_USER_RG];
    [defaults setObject:user.phone forKey:STORAGE_USER_PHONE];
    
    if(user.addressIds){
        [defaults setInteger:[user.addressIds count]forKey:STORAGE_USER_ADDRESSES_ID_SIZE];
        for(int count = 0; count <  [user.addressIds count] ; count++){
                [defaults setObject:[user.addressIds objectAtIndex:count] forKey:[NSString stringWithFormat:@"%@%d",STORAGE_USER_ADDRESSES_ITEM,count]];
        }
    }
    
    return [defaults synchronize];
}
+(BOOL) deleteSaveUser{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:nil forKey:STORAGE_USER_ID];
    [defaults setObject:nil forKey:STORAGE_USER_NAME];
    [defaults setObject:nil forKey:STORAGE_USER_EMAIL];
    [defaults setObject:nil forKey:STORAGE_USER_TOKEN];
    [defaults setObject:nil forKey:STORAGE_USER_CPF];
    [defaults setObject:nil forKey:STORAGE_USER_RG];
    [defaults setObject:nil forKey:STORAGE_USER_PHONE];
    [defaults setInteger:0 forKey:STORAGE_USER_ADDRESSES_ID_SIZE];
    
    return [defaults synchronize];
}
+(User*) getUserLogged{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    TrayUser * user = [TrayUser new];
    
    user.id = [defaults objectForKey:STORAGE_USER_ID];
    user.name = [defaults objectForKey:STORAGE_USER_NAME];
    user.email = [defaults objectForKey:STORAGE_USER_EMAIL];
    user.token = [defaults objectForKey:STORAGE_USER_TOKEN];
    user.personalID = [defaults objectForKey:STORAGE_USER_CPF];
    user.personalRg = [defaults objectForKey:STORAGE_USER_RG];
    user.phone = [defaults objectForKey:STORAGE_USER_PHONE];


    int size = (int)[defaults integerForKey:STORAGE_USER_ADDRESSES_ID_SIZE];
    
    NSMutableArray * itens = [NSMutableArray new];
    for(int count = 0; count <  size ; count++){
        NSString* id =  [defaults objectForKey:[NSString stringWithFormat:@"%@%d",STORAGE_USER_ADDRESSES_ITEM,count]];
        [itens addObject:id];
    }
   
    if([itens count] > 0){
        user.addressIds = itens;
    }
    
    return user;
}

@end
