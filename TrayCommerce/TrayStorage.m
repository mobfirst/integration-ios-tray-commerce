//
//  TrayStorage.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/12/14.
//  Copyright (c) 2014 Vitrina. All rights reserved.
//

#import "TrayStorage.h"

#define STORAGE_CART_PRODUCT_SIZE @"STORAGE_CART_PRODUCT_SIZE"
#define CARACTER_SEPARATOR @ ";"
#define STORAGE_CART_PRODUCT @"STORAGE_CART_PRODUCT_"
#define STORAGE_CART_VALUE @"STORAGE_CART_VALUE"


#define STORAGE_PAYMENTS_METHODS_ID  @"STORAGE_PAYMENTS_METHODS_ID"
#define STORAGE_PAYMENTS_METHODS_NAME  @"STORAGE_PAYMENTS_METHODS_NAME"
#define STORAGE_PAYMENTS_METHODS_IDENTIFIER  @"STORAGE_PAYMENTS_METHODS_IDENTIFIER"
#define STORAGE_PAYMENTS_METHODS_OPERATOR_NAME @"STORAGE_PAYMENTS_METHODS_OPERATOR_NAME"
#define STORAGE_PAYMENTS_METHODS_PLOTS  @"STORAGE_PAYMENTS_METHODS_PLOTS"
#define STORAGE_PAYMENTS_METHODS_CREDIT  @"STORAGE_PAYMENTS_METHODS_CREDIT"
#define STORAGE_PAYMENTS_METHODS_CHECKOUT_ID  @"STORAGE_PAYMENTS_METHODS_CHECKOUT_ID"

#define STORAGE_SHIPPING_METHODS_ID @"STORAGE_SHIPPING_METHODS_ID"
#define STORAGE_SHIPPING_METHODS_NAME @"STORAGE_SHIPPING_METHODS_NAME"
#define STORAGE_SHIPPING_METHODS_COST @"STORAGE_SHIPPING_METHODS_COST"

#define STORAGE_CREDIT_CART_OWNER @"STORAGE_CREDIT_CART_OWNER"
#define STORAGE_CREDIT_CART_NUMBER @"STORAGE_CREDIT_CART_NUMBER"
#define STORAGE_CREDIT_CART_MONTH @"STORAGE_CREDIT_CART_MONTH"
#define STORAGE_CREDIT_CART_YEAR @"STORAGE_CREDIT_CART_YEAR"
#define STORAGE_CREDIT_CART_CVV @"STORAGE_CREDIT_CART_CVV"
#define STORAGE_CREDIT_CART_SPLIT @"STORAGE_CREDIT_CART_SPLIT"

@implementation TrayStorage

#pragma ADDRESS
+(BOOL) saveChoicedAddress: (Address*) address{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:address.id forKey:ADDRESSES_ID];
    [defaults setObject:address.addressLine1 forKey:ADDRESSES_LINE_1];
    [defaults setObject:address.addressLine2 forKey:ADDRESSES_LINE_2];
    [defaults setObject:address.neighborhood forKey:ADDRESSES_NEIGHBORHOOD];
    [defaults setObject:address.number forKey:ADDRESSES_NUMBER];
    [defaults setObject:address.postalCode forKey:ADDRESSES_POSTAL_CODE];
    [defaults setObject:address.city forKey:ADDRESSES_CITY];
    [defaults setObject:address.state forKey:ADDRESSES_STATE];

    return [defaults synchronize];
}
+(Address*) getChoicedAddress{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    Address * address = [Address new];
    address.id = [defaults objectForKey:ADDRESSES_ID];
    address.addressLine1 = [defaults objectForKey:ADDRESSES_LINE_1];
    address.addressLine2 = [defaults objectForKey:ADDRESSES_LINE_2];
    address.number = [defaults objectForKey:ADDRESSES_NUMBER];
    address.neighborhood = [defaults objectForKey:ADDRESSES_NEIGHBORHOOD];
    address.postalCode = [defaults objectForKey:ADDRESSES_POSTAL_CODE];
    address.city = [defaults objectForKey:ADDRESSES_CITY];
    address.state = [defaults objectForKey:ADDRESSES_STATE];
    address.country = ADDRESSES_COUNTRY_CODE;
    
    return address;
}

#pragma CART
+(BOOL) saveCart: (Cart*) cart;{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject: cart.value forKey:STORAGE_CART_VALUE];
    [defaults setInteger:[cart.products count] forKey:STORAGE_CART_PRODUCT_SIZE];
    
    for(int count = 0; count < [cart.products count] ; count++){
        ProductCart* product = [cart.products objectAtIndex:count];
        NSString* stringPtoductCart = [self convertProductCartOnTrayCartProductString:product];        
        [defaults setObject:stringPtoductCart forKey:[NSString stringWithFormat:@"%@%d",STORAGE_CART_PRODUCT,count]];
    }
    
    return [defaults synchronize];
}
+(NSString*) convertProductCartOnTrayCartProductString:(ProductCart*) product{
    
    NSString* variantId =@"0";
    
    
    if(product.option != nil && [product.option.childrens count] > 0){
        Variation* variation = [product.option.childrens objectAtIndex:0];
        variantId = variation.id;
    }
    
    return [NSString stringWithFormat:@"%@%@%lu%@%@%@%@%@%@",product.id,CARACTER_SEPARATOR,(long)product.quantity,CARACTER_SEPARATOR,
            product.price,CARACTER_SEPARATOR,variantId,CARACTER_SEPARATOR, product.name];
}
+(NSArray*) getUserCarts{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    int size = (int) [defaults integerForKey:STORAGE_CART_PRODUCT_SIZE];
    
    NSMutableArray* itens = [NSMutableArray new];
    
    for(int count = 0; count < size; count ++){
        
        TrayCartObject * cartObject = [self convertStringOnTrayCartObject:[defaults objectForKey:[NSString stringWithFormat:@"%@%d",STORAGE_CART_PRODUCT,count]]];
        
        if(cartObject){
            [itens addObject:cartObject];
        }
    }
    return itens;
}
+(TrayCartObject*) convertStringOnTrayCartObject:(NSString*) string{

    if(!string || [string rangeOfString:CARACTER_SEPARATOR].location == NSNotFound){
        return nil;
    }
    
    NSArray* itens = [string componentsSeparatedByString:CARACTER_SEPARATOR];

    if([itens count] != 5){
        return nil;
    }
    
    TrayCartObject* cartObject = [TrayCartObject new];
    cartObject.productId = [itens objectAtIndex:0];
    cartObject.quantity = [itens objectAtIndex:1];
    cartObject.price = [itens objectAtIndex:2];
    cartObject.variant = [itens objectAtIndex:3];
    cartObject.productName = [itens objectAtIndex:4];
    
    return cartObject;
    
}

+(NSString*) getCartValue{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:STORAGE_CART_VALUE];
}

#pragma PAYMENTS METHODS
+(BOOL) savePaymentsMethods: (TrayPaymentMethod*) paymentMethod{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if(paymentMethod){
        [defaults setObject:paymentMethod.id forKey:STORAGE_PAYMENTS_METHODS_ID];
        [defaults setObject:paymentMethod.name forKey:STORAGE_PAYMENTS_METHODS_NAME];
        [defaults setObject:paymentMethod.identifier forKey:STORAGE_PAYMENTS_METHODS_IDENTIFIER];
        [defaults setObject:paymentMethod.operatorName forKey:STORAGE_PAYMENTS_METHODS_OPERATOR_NAME];
        [defaults setObject:paymentMethod.maxPlots forKey:STORAGE_PAYMENTS_METHODS_PLOTS];
        [defaults setBool:paymentMethod.isCredit forKey:STORAGE_PAYMENTS_METHODS_CREDIT];
        [defaults setObject:paymentMethod.checkoutId forKey:STORAGE_PAYMENTS_METHODS_CHECKOUT_ID];
    }
    else{
        [defaults removeObjectForKey:STORAGE_PAYMENTS_METHODS_ID];
        [defaults removeObjectForKey:STORAGE_PAYMENTS_METHODS_NAME];
        [defaults removeObjectForKey:STORAGE_PAYMENTS_METHODS_IDENTIFIER];
        [defaults removeObjectForKey:STORAGE_PAYMENTS_METHODS_OPERATOR_NAME];
        [defaults removeObjectForKey:STORAGE_PAYMENTS_METHODS_PLOTS];
        [defaults removeObjectForKey:STORAGE_PAYMENTS_METHODS_CREDIT];
        [defaults removeObjectForKey:STORAGE_PAYMENTS_METHODS_CHECKOUT_ID];
    }
    return [defaults synchronize];
}
+(TrayPaymentMethod*) getPaymentsMethods{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    TrayPaymentMethod * paymentMethod = [TrayPaymentMethod new];
    
    paymentMethod.id  = [defaults objectForKey:STORAGE_PAYMENTS_METHODS_ID];

    if(!paymentMethod.id){
        return nil;
    }
    
    paymentMethod.name = [defaults objectForKey:STORAGE_PAYMENTS_METHODS_NAME];
    paymentMethod.identifier = [defaults objectForKey:STORAGE_PAYMENTS_METHODS_IDENTIFIER];
    paymentMethod.operatorName  = [defaults objectForKey:STORAGE_PAYMENTS_METHODS_OPERATOR_NAME];
    paymentMethod.maxPlots = [defaults objectForKey:STORAGE_PAYMENTS_METHODS_PLOTS];
    paymentMethod.isCredit = [defaults boolForKey:STORAGE_PAYMENTS_METHODS_CREDIT];
    paymentMethod.checkoutId = [defaults objectForKey:STORAGE_PAYMENTS_METHODS_CHECKOUT_ID];
    
    return paymentMethod;
}


#pragma SHIPPING METHODS
+(BOOL) saveShippingMethods: (ShippingMethod*) shippingMethod{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:shippingMethod.id forKey:STORAGE_SHIPPING_METHODS_ID];
    [defaults setObject:shippingMethod.title forKey:STORAGE_SHIPPING_METHODS_NAME];
    [defaults setObject:shippingMethod.value forKey:STORAGE_SHIPPING_METHODS_COST];
    
    return [defaults synchronize];
}
+(ShippingMethod*) getShippingMethods{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    ShippingMethod* method = [ShippingMethod new];
    
    method.id = [defaults objectForKey:STORAGE_SHIPPING_METHODS_ID];
    
    if(!method.id){
        return nil;
    }
    
    method.value = [defaults objectForKey:STORAGE_SHIPPING_METHODS_COST];
    method.title = [defaults objectForKey:STORAGE_SHIPPING_METHODS_NAME];
    
    return method;
}

+(BOOL) saveUserCreditCard:(CreditCard*) creditCard{
   
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:creditCard.ownerName forKey:STORAGE_CREDIT_CART_OWNER];
    [defaults setObject:creditCard.cvv forKey:STORAGE_CREDIT_CART_CVV];
    [defaults setObject:creditCard.split forKey:STORAGE_CREDIT_CART_SPLIT];
    
    [defaults setObject:creditCard.cardNumber forKey:STORAGE_CREDIT_CART_NUMBER];
    [defaults setObject:creditCard.monthExpirateDate forKey:STORAGE_CREDIT_CART_MONTH];
    [defaults setObject:creditCard.yearExpirateDate forKey:STORAGE_CREDIT_CART_YEAR];
    
    return [defaults synchronize];
    
}
+(CreditCard*) getUserCreditCard{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    CreditCard * creditCart = [CreditCard new];
    
    creditCart.cvv = [defaults objectForKey:STORAGE_CREDIT_CART_CVV];
    creditCart.ownerName = [defaults objectForKey:STORAGE_CREDIT_CART_OWNER];
    creditCart.split = [defaults objectForKey:STORAGE_CREDIT_CART_SPLIT];
    creditCart.cardNumber = [defaults objectForKey:STORAGE_CREDIT_CART_NUMBER];
    creditCart.monthExpirateDate = [defaults objectForKey:STORAGE_CREDIT_CART_MONTH];
    creditCart.yearExpirateDate = [defaults objectForKey:STORAGE_CREDIT_CART_YEAR];
    return creditCart;
}

@end
