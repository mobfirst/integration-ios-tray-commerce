//
//  TrayStorage.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/12/14.
//  Copyright (c) 2014 Vitrina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrayPaymentMethod.h"
#import "ShippingMethod.h"
#import "Address.h"
#import "CreditCard.h"
#import "TrayConstants.h"
#import "ProductCart.h"
#import "Variation.h"
#import "Cart.h"
#import "TrayCartObject.h"

@interface TrayStorage : NSObject

#pragma PAYMENTS METHODS
+(BOOL) savePaymentsMethods: (PaymentMethod*) paymentMethod;
+(TrayPaymentMethod*) getPaymentsMethods;

#pragma SHIPPING METHODS
+(BOOL) saveShippingMethods: (ShippingMethod*) shippingMethod;
+(ShippingMethod*) getShippingMethods;

#pragma ADDRESS
+(BOOL) saveChoicedAddress: (Address*) address;
+(Address*) getChoicedAddress;

#pragma CART
+(BOOL) saveCart: (Cart*) cart;
+(NSArray*) getUserCarts;
+(NSString*) getCartValue;

#pragma CARD
+(BOOL) saveUserCreditCard:(CreditCard*) creditCard;
+(CreditCard*) getUserCreditCard;




@end
