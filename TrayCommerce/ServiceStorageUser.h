//
//  ServiceStorageUser.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/12/14.
//  Copyright (c) 2014 Vitrina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrayUser.h"
#import "TrayConstants.h"

@interface ServiceStorageUser : NSObject

+(BOOL) userIsLogged;
+(BOOL) saveUser:(User*)user;
+(BOOL) deleteSaveUser;
+(User*) getUserLogged;

@end
