//
//  TrayMappers.m
//  TrayCommerce
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "TrayMappers.h"
#import "TrayPaymentsValue.h"
#import "NSString+HTML.h"
#import "TrayCheckoutPayment.h"


#include <ifaddrs.h>
#include <arpa/inet.h>

#define HTTP @"http"
#define HTTPS @"https"

#define ID @"id"
#define NAME @"name"
#define EMAIL @"email"
#define AVAILABILITY @"availability"
#define AVAILABILITY_LEAD @"Sob consulta"
#define AVAILABILITY_PRICE @"0.00"



#define ADDRESS @"address"
#define ADDRESS_NUMBER @"address_number"
#define POSTAL_CODE @"postal_code"
#define CITY @"city"
#define STATE @"state"
#define COUNTRY @"country"

#define CATEGORY @"Category"

#define PRODUCTS @"Products"
#define PRODUCT @"Product"

#define DESCRIPTION @"description"

#define VARIANTS @"Variants"
#define VARIANT @"Variant"



#define  CUSTOMERADDRESSES @"CustomerAddress"

@implementation TrayMappers


+(NSDictionary*) convertNSStringToDictionary:(NSString*) string{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    if(!data){
        return nil;
    }
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return json;
}
+(id) checkIfInformationIsNil:(id) information{
    return [information isKindOfClass:[NSNull class]] ? nil : information;
}

+(id) getValue:(NSString*) value fromDictionary:(id) dictionary{
    id data = [dictionary objectForKey:value];
    return [self checkIfInformationIsNil:data];
}


#pragma MobFirst

+(NSString*) getMobFistToken:(NSString*) apiResponse{
    
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    NSString* ACCESS_TOKEN = @"access_token";
    
    if(!json){
        return nil;
    }
    
    NSString* token =  [json objectForKey:ACCESS_TOKEN];
    return [self checkIfInformationIsNil:token];
}


#pragma TOKEN

+(NSString*) getTrayRefreshToken:(NSString*) apiResponse
{
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    return [self getValue:HEADER_TOKEN fromDictionary:json];
}

+(NSString*) getTrayAccessToken:(NSString*) apiResponse
{
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    return [self getValue:HEADER_TOKEN fromDictionary:json];
}

#pragma Store Information

+(BOOL) getStoreCustomSettings:(id) dictionary{
    

    if(!dictionary){
        return NO;
    }
    NSString* TRAY_STORE_CODE = @"TrayCommerceStoreCode";
    NSString* TRAY_STORE_API = @"TrayCommerceStoreApi";
    NSString* TRAY_CHECKOUT_TOKEN = @"TrayCheckoutToken";
    NSString* TRAY_STORE_TOKEN = @"TrayCommerceAccessToken";
    
    [ServiceApiMapper Api].API_URL = [NSString stringWithFormat:@"%@/",[self getValue:TRAY_STORE_API fromDictionary:dictionary]];
    [ServiceApiMapper Api].API_CODE = [self getValue:TRAY_STORE_CODE fromDictionary:dictionary];
    [ServiceApiMapper Api].API_TOKEN = [self getValue:TRAY_STORE_TOKEN fromDictionary:dictionary];
    [ServiceApiMapper Api].MAX_INSTALLMENTS = [self getValue:@"TrayCheckoutMaxInstallments" fromDictionary:dictionary];
    [ServiceApiMapper Api].API_TOKEN_CHECKOUT = [self getValue:TRAY_CHECKOUT_TOKEN fromDictionary:dictionary];
    return YES;

}

+(BOOL) storageStoreConfiguration:(NSString*) response{
    [ServiceApiMapper Api].STORE_CONFIG = response;
    return true;
}

+(NSDictionary*) mapperStoreConfiguration:(NSString*) key{
    
    NSDictionary * json = [self convertNSStringToDictionary:[ServiceApiMapper Api].STORE_CONFIG];
    
    if(!json){
        return nil;
    }

    id configurations = [self getValue:@"Configurations" fromDictionary:json];
    
    if(configurations){
        for(id configuration in configurations){
            id configurationItem = [self getValue:@"Configuration" fromDictionary:configuration];
            if( [[configurationItem objectForKey:@"registry"] isEqualToString:key]){
                id value = [configurationItem objectForKey:@"value"];
                return @{key:value};
            }
        }
    }
    return nil;
}

#pragma Category

+(NSArray*) getCategoriesFromDictionary:(id) dictionary{
    
    NSMutableArray* array = [NSMutableArray new];
    
    for (id object in dictionary) {
        
        id categoryJson = [object objectForKey:CATEGORY];
        
        ProductCategory* category = [ProductCategory new];
        
        category.id = [categoryJson objectForKey:ID];
        category.name = [categoryJson objectForKey:NAME];
        
        id children = [categoryJson objectForKey:@"children"];
        
        if(children && [children isKindOfClass:[NSArray class]] ){
            category.childrens = [NSMutableArray arrayWithArray:[self getCategoriesFromDictionary:children]];
        }
        
        if(category.id){
            [array addObject:category];
        }
    }
    
    return  array;
}

+(NSArray*) convertToLocalCategories:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSArray* idList = [json objectForKey:CATEGORY];
    
    return [self getCategoriesFromDictionary:idList];
}

#pragma Products

+(NSMutableArray*) getProductImages:(id) dictionary{
    
    NSMutableArray* arrayImage = [NSMutableArray new];
    
    for (id object in dictionary) {
        [arrayImage addObject:[object objectForKey:HTTPS]];
    }
    
    return arrayImage;
}

+(Product*) getProductFromDictionary:(id) dictionary{
        
    NSString* availability = [dictionary objectForKey:AVAILABILITY];
    NSString* price = [dictionary objectForKey:@"price"]; //
    
    
    if(availability && [availability isEqualToString:AVAILABILITY_LEAD] && price && [price isEqualToString:AVAILABILITY_PRICE]){
        return nil;
    }
    
    Product* product = [Product new];
    product.id =  [dictionary objectForKey:ID];
    product.name =  [dictionary objectForKey:NAME];
    
    product.productDescription = [dictionary objectForKey:@"description"];
    
    if(product.productDescription){
    
        product.productDescription = [product.productDescription decodeHTMLCharacterEntities];
        
        NSString* descriptionClean = [product.productDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
        descriptionClean = [descriptionClean stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        descriptionClean = [descriptionClean stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        descriptionClean = [descriptionClean stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        descriptionClean = [descriptionClean stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if([descriptionClean length] == 0){
            product.productDescription = nil;
        }
    }
    
    product.price = price;
    
    product.specialPrice = [self getSpecialPrice:dictionary];

    product.images = [self getProductImages:[dictionary objectForKey:@"ProductImage"]];
    
    id variant = [dictionary objectForKey:VARIANT];
    
    if(variant && [variant isKindOfClass:[NSArray class]] && [variant count] > 0){
        Option * option = [Option new];
        option.name = @"Opções";
        option.required = YES;
        product.option = option;
    }
    
    if([dictionary objectForKey:@"virtual_product"]){
        product.virtualProduct = [[NSString stringWithFormat:@"%@",[dictionary objectForKey:@"virtual_product"]] isEqualToString:@"1"];
    }
    
    return product;
}


+(NSString*) getSpecialPrice:(id) dictionary{
    
    NSString* specialPrice = [dictionary objectForKey:@"promotional_price"];
    if(![specialPrice isEqualToString:@"0.00"]){
        
        NSString* endPromotion = [self getValue:@"end_promotion" fromDictionary:dictionary];
        
        if(endPromotion){
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            
            NSDate* endPrmotionDate = [dateFormatter dateFromString:endPromotion];
            
            NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
            [offsetComponents setDay:1];
            endPrmotionDate = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] dateByAddingComponents:offsetComponents toDate: endPrmotionDate options:0];
            NSDate* today = [NSDate date];
            if ([today compare:endPrmotionDate] == NSOrderedAscending) {
                return specialPrice;
            }
        }
        else{
            return specialPrice;
        }
    }
    
    return nil;
    
}

+(NSArray*) getProductsFromDictionary:(id) dictionary{
    
    NSMutableArray* array = [NSMutableArray new];
    
    for (id object in dictionary) {
        
        Product * product = [self getProductFromDictionary:[object objectForKey:PRODUCT]];
        if(product){
            [array addObject:product];
        }
    }
    return array;
}

+(NSArray*) convertToLocalProducts:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSArray* idList = [json objectForKey:PRODUCTS];
    
    return  [self getProductsFromDictionary:idList];
}

+(Product*) convertToLocalProduct:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    return [self getProductFromDictionary:[json objectForKey:PRODUCT]];
}

+(NSString*) getSkuFromVariants: (id)  dictionary{
    
    NSString* value = @"";
    
    for (id object in dictionary) {
        value = [NSString stringWithFormat:@"%@%@ : %@ - ",value,[object objectForKey:@"type"],[object objectForKey:@"value"]];
    }
    
    if([value length] > 0){
        value = [value substringToIndex:[value length]-2];
    }
    return value;
}

+(Variation*) getVariantsFromDictionary:(id)  dictionary{
    
    Variation* variation = [Variation new];
    
    variation.id = [dictionary objectForKey:ID];
    variation.specialPrice = [self getSpecialPrice:dictionary];
    variation.price = [dictionary objectForKey:@"price"];
    
    variation.value = [self getSkuFromVariants: [dictionary objectForKey:@"Sku"]];
    
    return variation;
}

+(NSArray*) convertToLocalProductVariants:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSArray* idList = [json objectForKey:VARIANTS];
    
    NSMutableArray* array = [NSMutableArray new];
    
    for (id object in idList) {
        [array addObject:[self getVariantsFromDictionary:[object objectForKey:VARIANT]]];
    }
    
    return array;
}

#pragma User

+(User*) confirmUserCreate: (User*) user onResponse:(NSString*) apiResponse;{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSString* MESSAGE = @"message";
    NSString* CREATED = @"Created";
    
    NSString* message = [json objectForKey:MESSAGE];
    message = [self checkIfInformationIsNil:message];
    
    if(!message || ![message isEqualToString:CREATED]){
        return nil;
    }
    
    NSString* id = [json objectForKey:ID];
    id = [self checkIfInformationIsNil:id];
    
    if(!id){
        return nil;
    }
    
    user.id = id;
    
    return user;
    
}

+(User*) convertToLocalUser:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSString* PHONE = @"phone";
    NSString* CPF = @"cpf";
    NSString* RG = @"rg";
    NSString* TOKEN = @"token";
    
    
    id objectCustomer = [self getValue:CUSTOMER fromDictionary:json];
    
    if(!objectCustomer){
        return nil;
    }
    
    TrayUser* user = [TrayUser new];
    user.id = [self getValue:ID fromDictionary:objectCustomer];
    user.name = [self getValue:NAME fromDictionary:objectCustomer];
    user.email = [self getValue:EMAIL fromDictionary:objectCustomer];
    user.phone = [self getValue:PHONE fromDictionary:objectCustomer];
    user.personalRg = [self getValue:RG fromDictionary:objectCustomer];
    user.personalID = [self getValue:CPF fromDictionary:objectCustomer];
    user.token = [self getValue:TOKEN fromDictionary:objectCustomer];
    
    id addressesObjects = [self getValue:CUSTOMERADDRESSES fromDictionary:objectCustomer];
    
    if(addressesObjects && [addressesObjects isKindOfClass:[NSArray class]]){
        
        NSMutableArray * itens = [NSMutableArray new];
        
        for(id itemAdress in addressesObjects){
            
            NSString* id = [itemAdress objectForKey:ID];
            if(id) {
                [itens addObject:id];
            }
        }
        user.addressIds = itens;
    }
    
    return user;
}

+(NSString*) convertToUserId:(NSString*) apiResponse{
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    return [self getValue:ID fromDictionary:json];
}

#pragma Addresses
+(NSArray*) convertToLocalCustomerAddresses:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    id addressObject = [self getValue:CUSTOMERADDRESSES fromDictionary:json];
    
    if(!addressObject){
        return nil;
    }
    
    
    Address * address = [Address new];
    
    address.id = [self getValue:ID fromDictionary:addressObject];
    
    if(!address.id){
        return nil;
    }
    address.addressLine1 = [self getValue:ADDRESSES_LINE_1 fromDictionary:addressObject];
    address.addressLine2 = [self getValue:ADDRESSES_LINE_2 fromDictionary:addressObject];
    address.number = [self getValue:ADDRESSES_NUMBER fromDictionary:addressObject];
    address.neighborhood = [self getValue:ADDRESSES_NEIGHBORHOOD fromDictionary:addressObject];
    address.postalCode = [self getValue:ADDRESSES_POSTAL_CODE fromDictionary:addressObject];
    address.city = [self getValue:ADDRESSES_CITY fromDictionary:addressObject];
    address.state = [self getValue:ADDRESSES_STATE fromDictionary:addressObject];
    address.country = ADDRESSES_COUNTRY_CODE;
    
    NSMutableArray * addresses = [NSMutableArray new];
    
    [addresses addObject:address];
    
    return addresses;
}

+(NSString*) confirmAddressCreate:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSString* MESSAGE = @"message";
    NSString* CREATED = @"Created";
    
    NSString* message = [json objectForKey:MESSAGE];
    message = [self checkIfInformationIsNil:message];
    
    if(!message || ![message isEqualToString:CREATED]){
        return nil;
    }
    
    NSString* id = [json objectForKey:ID];
    return [self checkIfInformationIsNil:id];
}

#pragma Cart
+(NSArray*) convertTolocalCart:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSString* CARTS = @"Carts";
    
    id itensOnCart = [self getValue:CARTS fromDictionary:json];
    
    if(!itensOnCart){
        return nil;
    }
    
    NSMutableArray * itensList = [NSMutableArray new];
    
    for(id item in itensOnCart){
        TrayCartObject* cart = [self getTrayCartObjectFromDictionary: item];
        if(cart){
            [itensList addObject:cart];
        }
    }
    return itensList;
}

+(TrayCartObject*) getTrayCartObjectFromDictionary:(id) dictionary{
    
    NSString* CART = @"Cart";
    
    NSString* PRODUCT_ID = @"product_id";
    NSString* SESSION_ID = @"session_id";
    NSString* QUANTITY = @"quantity";
    NSString* PRICE = @"price";
    NSString* VARIANT_ID = @"variant_id";
    
    id dicCart = [self getValue:CART fromDictionary:dictionary];
    
    if(!dicCart){
        return nil;
    }
    
    TrayCartObject * cart = [TrayCartObject new];
    
    cart.token = [self getValue:SESSION_ID fromDictionary:dicCart];
    cart.productId = [self getValue:PRODUCT_ID fromDictionary:dicCart];
    cart.quantity = [self getValue:QUANTITY fromDictionary:dicCart];
    cart.price = [self getValue:PRICE fromDictionary:dicCart];
    cart.variant = [self getValue:VARIANT_ID fromDictionary:dicCart];
    
    
    return cart;
}

+(Cart*) convertTolocalCartItems:(NSArray*) cartObjects products:(NSArray*) products variations:(NSArray*) variations
{
    if([cartObjects count] == 0 || [products count] == 0){
        return nil;
    }
    
    double valueCart = 0;
    
    NSMutableArray* cartProducts = [NSMutableArray new];
    
    for(TrayCartObject * cartItem in cartObjects){
        valueCart+= [cartItem.price doubleValue] * [cartItem.quantity doubleValue];
        
        
        for(Product * product in products){
            
            if([product.id isEqualToString:cartItem.productId]){
                
                ProductCart* productCart = [ProductCart new];
                
                
                if(![cartItem.variant isEqualToString:@"0"]){
                    
                    for( Variation * variation in variations){
                        
                        if([variation.id isEqualToString:cartItem.variant]){
                            
                            Option * option = [Option new];
                            option.id = cartItem.token;
                            option.name = @"Opções";
                            
                            NSMutableArray* optionVariants = [NSMutableArray new];
                            
                            [optionVariants addObject:variation];
                            option.childrens = optionVariants;
                            
                            productCart.option = option;
                            
                            break;
                        }
                    }
                }
                
                productCart.images =  product.images;
                productCart.name = product.name;
                productCart.price = cartItem.price;
                productCart.quantity = [cartItem.quantity integerValue];
                productCart.id = cartItem.productId;
                productCart.virtualProduct = product.virtualProduct;
                
                [cartProducts addObject:productCart];
                break;
            }
        }
    }
    
    Cart * cart = [Cart new];
    
    cart.value = [NSString stringWithFormat:@"%f",valueCart];
    cart.id =  ((TrayCartObject*)[cartObjects objectAtIndex:0]).token;
    cart.products =  cartProducts;
    
    return cart;
}


#pragma Payments Methods
+(NSArray*) convertToLocalPaymentsMethods:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSString* PAYMENTSMETHODS = @"PaymentMethods";
    NSString* CREDIT = @"credit";
    NSString* ORDER_PAYMENTS = @"order";
    
    id objectPayments = [self getValue:PAYMENTSMETHODS fromDictionary:json];
    
    if(!objectPayments){
        return  nil;
    }
    
    
    NSMutableArray* paymentsMethods = [NSMutableArray new];
    
    id objectsCredits = [self getValue:CREDIT fromDictionary:objectPayments];
    
    if(objectsCredits){
        
        for(id dic in objectsCredits){
            
            PaymentMethod* method = [self getPaymentMethodFromDictionary:dic];
            if(method){
                [paymentsMethods addObject:method];
            }
        }
    }
    
    id objectsOrder = [self getValue:ORDER_PAYMENTS fromDictionary:objectPayments];
    
    if(objectsOrder){
        for(id dic in objectsOrder){
            PaymentMethod* method = [self getPaymentMethodFromDictionary:dic];
            if(method){
                [paymentsMethods addObject:method];
            }
        }
        
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    NSMutableArray* methods = [NSMutableArray new];
    [methods addObjectsFromArray:[paymentsMethods sortedArrayUsingDescriptors:@[sort]]];
    
    return methods;
    
}


+(PaymentMethod*) getPaymentMethodFromDictionary:(id) dictionary{
    
    TrayPaymentMethod * paymentsMethods = [TrayPaymentMethod new];
    
    NSString* DISPLAY_NAME = @"display_name";
    NSString* OPERATOR_NAME = @"operator_name";
    NSString* IDENTIFIER = @"identifier";
    NSString* MAX_PLOTS = @"max_plots";
    NSString* TRAYCHECKOUT = @"traycheckout";
    
    paymentsMethods.id = [self getValue:ID fromDictionary:dictionary];
    
    if(!paymentsMethods.id){
        return nil;
    }
    
    paymentsMethods.identifier = [self getValue:IDENTIFIER fromDictionary:dictionary];
    
    if(!paymentsMethods.identifier){
        return nil;
    }
    
    if([[paymentsMethods.identifier lowercaseString] rangeOfString:TRAYCHECKOUT].location == NSNotFound) {
        return nil;
    }
    
    paymentsMethods.name = [self getValue:DISPLAY_NAME fromDictionary:dictionary];
    if(paymentsMethods.name){
        NSArray* names = [paymentsMethods.name componentsSeparatedByString:@"-"];
        paymentsMethods.name = [names objectAtIndex:0];
    }
    
    //INSERT: TRAY COMMERCE
    if([[paymentsMethods.name lowercaseString] rangeOfString:@"saldo virtual"].location != NSNotFound) {
        return nil;
    }
    
    if([[paymentsMethods.name lowercaseString] rangeOfString:@"boleto bradesco"].location != NSNotFound) {
        return nil;
    }

    if([[paymentsMethods.name lowercaseString] rangeOfString:@"cartão maestro"].location != NSNotFound) {
        return nil;
    }


    TrayCheckoutPayment* payment = [self getTrayCheckoutPaymentId:paymentsMethods.identifier];
    
    if(!payment){
        return nil;
    }
    
    
    paymentsMethods.maxPlots = [self getValue:MAX_PLOTS fromDictionary:dictionary];
    paymentsMethods.operatorName = [self getValue:OPERATOR_NAME fromDictionary:dictionary];
    paymentsMethods.isCredit = payment.credit;
    paymentsMethods.checkoutId = payment.trayCheckoutId;
    
    return paymentsMethods;
    
}

+(TrayCheckoutPayment*) getTrayCheckoutPaymentId:(NSString*) paymentId{
    
    
    NSArray* payments = [self getTrayCheckoutPayments];
    
    for(TrayCheckoutPayment* payment in payments){
        
        if([payment.trayCommerceId isEqualToString:paymentId]){
            return payment;
        }
    }
    return nil;
}


+(NSArray*) getTrayCheckoutPayments{
    
    NSMutableArray* payments = [NSMutableArray new];
    
    [payments addObject:[self getTrayCheckoutPayment:@"CartaoDinersTrayCheckout" trayCheckoutId:@"2" isCredit:YES]]; //DINNERS
    [payments addObject:[self getTrayCheckoutPayment:@"CartaVisaTrayCheckout" trayCheckoutId:@"3" isCredit:YES]]; //VISA
    [payments addObject:[self getTrayCheckoutPayment:@"MasterCardTrayCheckout" trayCheckoutId:@"4" isCredit:YES]]; //MASTER
    [payments addObject:[self getTrayCheckoutPayment:@"CartaoAmexTrayCheckout" trayCheckoutId:@"5" isCredit:YES]]; //AMEX
    [payments addObject:[self getTrayCheckoutPayment:@"CartaoDiscoverTrayCheckout" trayCheckoutId:@"15" isCredit:YES]]; //DISCOVER
    [payments addObject:[self getTrayCheckoutPayment:@"CartaoEloTrayCheckout" trayCheckoutId:@"16" isCredit:YES]]; //ELO
    [payments addObject:[self getTrayCheckoutPayment:@"CartaoAuraTrayCheckout" trayCheckoutId:@"18" isCredit:YES]]; //AURA
    [payments addObject:[self getTrayCheckoutPayment:@"CartaoJCBTrayCheckout" trayCheckoutId:@"19" isCredit:YES]]; //JCB
    
    
    [payments addObject:[self getTrayCheckoutPayment:@"ItauShoplineTrayCheckout" trayCheckoutId:@"7" isCredit:NO]]; //TRANSFERENCIA ONLINE ITAU
    [payments addObject:[self getTrayCheckoutPayment:@"PeelaTrayCheckout" trayCheckoutId:@"14" isCredit:NO]]; //PEELA
    [payments addObject:[self getTrayCheckoutPayment:@"TransferenciaOnlineHSBCTrayCheckout" trayCheckoutId:@"21" isCredit:NO]]; //HSBC
    [payments addObject:[self getTrayCheckoutPayment:@"TransferenciaBradescoTrayCheckout" trayCheckoutId:@"22" isCredit:NO]]; //BRADESCO
    [payments addObject:[self getTrayCheckoutPayment:@"TransferenciaBBTrayCheckout" trayCheckoutId:@"23" isCredit:NO]]; //BB
    
    [payments addObject:[self getTrayCheckoutPayment:@"BoletoTrayCheckout" trayCheckoutId:@"6" isCredit:NO]]; //BOLETO
    
    return payments;
    
}


+(TrayCheckoutPayment*) getTrayCheckoutPayment:(NSString*) trayId trayCheckoutId:(NSString*) checkoutId isCredit:(BOOL) credit{
    
    TrayCheckoutPayment* payment = [TrayCheckoutPayment new];
    payment.trayCommerceId = trayId;
    payment.trayCheckoutId = checkoutId;
    payment.credit = credit;
    
    return payment;
    
}



#pragma Shipping Methods
+(NSArray*) convertToLocalShippingMethods:(NSString*) apiResponse
{
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSMutableArray * shippingsItens = [NSMutableArray new];
    
    NSString* SHIPPINGMETHODS = @"Shipping";
    NSString* COTATION = @"cotation";
    
    id shippingObjects = [self getValue:SHIPPINGMETHODS fromDictionary:json];
    
    if(!shippingObjects){
        return nil;
    }
    
    id cotationObject = [self getValue:COTATION fromDictionary:shippingObjects];
    
    for( id item in cotationObject){
        
        ShippingMethod* method = [self getShippingMethodFromDictionary:item];
        
        if(method){
            [shippingsItens addObject:method];
        }
    }
    
    return shippingsItens;
}


+(ShippingMethod*) getShippingMethodFromDictionary:(id) dictionary{
    
    
    ShippingMethod * method = [ShippingMethod new];
    
    NSString* VALUE = @"value";
    
    NSString* id= [self getValue:ID fromDictionary:dictionary];
    
    NSString* name = [self getValue:NAME fromDictionary:dictionary];
    NSString* price = [self getValue:VALUE fromDictionary:dictionary];
    
    if(!id || !name || !price){
        return nil;
    }
    
    method.id = id;
    method.title = name;
    method.value = [NSString stringWithFormat:@"%.02f",[price floatValue]];;
    
    return method;
}


#pragma Order
+(NSDictionary*) createOrderToTrayCommerce{
    
    
    TrayPaymentMethod * paymentMethod = [TrayStorage getPaymentsMethods];
    ShippingMethod* shippingMethod = [TrayStorage getShippingMethods];
    NSArray * itensOnCart = [TrayStorage getUserCarts];
    User * user = [ServiceStorageUser getUserLogged];
    Address * addressChoiced = [TrayStorage getChoicedAddress];
    
    /*
     * PRODUCTS SOLD
     */
    
    NSMutableArray * products = [NSMutableArray new];
    
    for(TrayCartObject * productCart in itensOnCart){
        
        NSDictionary*  product = @{ORDER_PRODUCT_SOLD_PRODUCT_ID : productCart.productId,
                                   ORDER_PRODUCT_SOLD_QUANTITY : productCart.quantity,
                                   ORDER_PRODUCT_SOLD_PRICE : [productCart.price stringByReplacingOccurrencesOfString:@"," withString:@"."],
                                   ORDER_PRODUCT_SOLD_ORIGINAL_PRICE : [productCart.price stringByReplacingOccurrencesOfString:@"," withString:@"."],
                                   ORDER_PRODUCT_SOLD_VARIANT_ID : productCart.variant
                                   };
        
        [products addObject:product];
    }
    
    
    /*
     * ADDRESS
     */
    
    
    NSDictionary * address = @{ ADDRESSES_LINE_1 : addressChoiced.addressLine1,
                                ADDRESSES_LINE_2 : addressChoiced.addressLine2,
                                ADDRESSES_POSTAL_CODE : addressChoiced.postalCode,
                                ADDRESSES_NUMBER : addressChoiced.number,
                                ADDRESSES_NEIGHBORHOOD : addressChoiced.neighborhood,
                                ADDRESSES_CITY : addressChoiced.city,
                                ADDRESSES_STATE : addressChoiced.state,
                                ADDRESSES_COUNTRY : ADDRESSES_COUNTRY_CODE
                                };
    
    
    NSMutableArray * addresses = [NSMutableArray new];
    [addresses addObject:address];
    
    
    /*
     * CUSTOMER
     */
    
    
    NSDictionary * customer = @{ ORDER_CUSTOMER_NAME : user.name,
                                 ORDER_CUSTOMER_CPF : user.personalID,
                                 ORDER_CUSTOMER_EMAIL : user.email,
                                 ORDER_CUSTOMER_PHONE : user.phone,
                                 USER_RG: user.personalRg,
                                 ORDER_CUSTOMER_ADDRESS : addresses
                                 };
    
    
    if(!shippingMethod){
        shippingMethod = [ShippingMethod new];
        shippingMethod.title = @"FRETE GRÁTIS";
        shippingMethod.value = @"0.00";
    }
    
    NSDictionary * objectOrder;
    
    if(!paymentMethod){
        
        objectOrder = @{ ORDER_PRODUCT_SOLD : products,
                         ORDER_POINT_SALE : ORDER_POINT_SALE_APP,
                         ORDER_SHIPPING : shippingMethod.title,
                         ORDER_SHIPPING_VALUE : shippingMethod.value,
                         ORDER_CUSTOMER : customer
                         };
        
    }else{
        
        objectOrder = @{ ORDER_PRODUCT_SOLD : products,
                         ORDER_POINT_SALE : ORDER_POINT_SALE_APP,
                         ORDER_SHIPPING : shippingMethod.title,
                         ORDER_SHIPPING_VALUE : shippingMethod.value,
                         ORDER_PAYMENT_FORM : paymentMethod.operatorName,
                         ORDER_PAYMENT_METHOD_ID : paymentMethod.id,
                         ORDER_CUSTOMER : customer
                         };
    }
    

    return @{ORDER: objectOrder};
}

+(NSString*) confirmCreateOrder:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSString* MESSAGE = @"message";
    NSString* CREATED = @"Created";
    NSString* ORDER_ID = @"order_id";
    
    NSString* message = [self getValue:MESSAGE fromDictionary:json];
    
    if(!message || ![message isEqualToString:CREATED]){
        return nil;
    }
    
    return [self getValue:ORDER_ID fromDictionary:json];
}

+(NSString*) getUrlCallback:(NSString*) apiResponse{
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSString* PAYMENT_NOTIFICATION = @"payments_notification";
    NSString* NOTIFICATION = @"notification";
    
    id order = [self getValue:ORDER fromDictionary:json];
    
    if(!order){
        return nil;
    }
    
    id paymentNotification = [self getValue:PAYMENT_NOTIFICATION fromDictionary:order];
    
    if(!paymentNotification){
        return nil;
    }
    return (NSString*)[self getValue:NOTIFICATION fromDictionary:paymentNotification];
}

+(NSDictionary*) getTrayCheckoutSimulateObject{
    
    NSString* TOKEN_ACCOUNT =@"token_account";
    NSString* PRICE = @"price";
    NSString* PAYMENT_ID = @"payment_method_id";
    
    TrayPaymentMethod * paymentMethod = [TrayStorage getPaymentsMethods];
    ShippingMethod* shippingMethod = [TrayStorage getShippingMethods];
    
    return @{
             TOKEN_ACCOUNT : [ServiceApiMapper Api].API_TOKEN_CHECKOUT,
             PRICE : [NSString stringWithFormat:@"%.02f",([[TrayStorage getCartValue] floatValue] + [shippingMethod.value floatValue])],
             PAYMENT_ID : paymentMethod.checkoutId,
             @"max_split_transaction" :  [ServiceApiMapper Api].MAX_INSTALLMENTS
             };
}

+(NSDictionary*) createOrderToTrayCheckoutWithUrl:(NSString*) urlCallback andOrderId:(NSString*) orderID{
    TrayPaymentMethod * paymentMethod = [TrayStorage getPaymentsMethods];
    ShippingMethod* shippingMethod = [TrayStorage getShippingMethods];
    NSArray * itensOnCart = [TrayStorage getUserCarts];
    User * user = [ServiceStorageUser getUserLogged];
    Address * addressChoiced = [TrayStorage getChoicedAddress];
    
    NSString* TOKEN_ACCOUNT = @"token_account";
    NSString* PRICE_SELLER = @"price_seller";

    NSString* CUSTOMER_CONTACTS = @"contacts";
    NSString* CUSTOMER_TYPE_CONTACT = @"type_contact";
    NSString* CUSTOMER_TYPE_CONTACT_MOBILE = @"M";
    NSString* CUSTOMER_TYPE_CONTACT_HOME = @"H";
    NSString* CUSTOMER_NUMBER_CONTACT = @"number_contact";
    NSString* CUSTOMER_ORDER = @"customer";

    NSString* ADDRESSES = @"addresses";
    NSString* ADDRESSES_TYPE_ADDRESS = @"type_address";
    NSString* ADDRESSES_TYPE_ADDRESS_SHIPPING = @"B";
    NSString* ADDRESSES_STREET = @"street";
    
    NSString* TRANSACTION_PRODUCT = @"transaction_product";
    NSString* TRANSACTION_PRODUCT_DESCRIPTION = @"description";
    NSString* TRANSACTION_PRODUCT_QUANTITY = @"quantity";
    NSString* TRANSACTION_PRODUCT_PRICE_UNIT = @"price_unit";
    NSString* TRANSACTION = @"transaction";
    
    NSString* TRANSACTION_SHIPPING_TYPE = @"shipping_type";
    NSString* TRANSACTION_SHIPPING_PRICE = @"shipping_price";
    NSString* TRANSACTION_SHIPPING_ORDER_NUMBER = @"order_number";
    NSString* TRANSACTION_SHIPPING_URL_NOTIFICATION = @"url_notification";
    NSString* TRANSACTION_IP_NUMBER =@ "customer_ip";
    
    NSString* PAYMENT = @"payment";
    NSString* PAYMENT_METHOD_ID = @"payment_method_id";
    NSString* PAYMENT_CARD_NAME = @"card_name";
    NSString* PAYMENT_CARD_NUMBER = @"card_number";
    NSString* PAYMENT_CARD_EXPIRATE_MONTH = @"card_expdate_month";
    NSString* PAYMENT_CARD_EXPIRATE_YEAR = @"card_expdate_year";
    NSString* PAYMENT_CARD_CVV = @"card_cvv";
    NSString* PAYMENT_CARD_SPLIT = @"split";
    
    
    
    /*
     *  CUSTOMER
     */
    
    NSMutableArray * contacts = [NSMutableArray new];
    
    
    NSString* phone = user.phone;
    phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSString* contactType = CUSTOMER_TYPE_CONTACT_HOME;
    
    if(phone.length > 3){
        NSString * prefixNumber = [phone substringWithRange:NSMakeRange(3, 1)];
        if([prefixNumber isEqualToString:@"7"] ||
           [prefixNumber isEqualToString:@"8"] ||
           [prefixNumber isEqualToString:@"9"] ){
            contactType = CUSTOMER_TYPE_CONTACT_MOBILE;
        }
    }
    
    NSDictionary * customer_contacts = @{
                                         CUSTOMER_TYPE_CONTACT : contactType,
                                         CUSTOMER_NUMBER_CONTACT : phone };
    
    [contacts addObject:customer_contacts];
    
    NSMutableArray * addresses = [NSMutableArray new];
    
    NSDictionary * customer_address = @{ ADDRESSES_TYPE_ADDRESS : ADDRESSES_TYPE_ADDRESS_SHIPPING,
                                         POSTAL_CODE : addressChoiced.postalCode,
                                         ADDRESSES_STREET : addressChoiced.addressLine1,
                                         ADDRESSES_NUMBER : addressChoiced.number,
                                         ADDRESSES_NEIGHBORHOOD : addressChoiced.neighborhood,
                                         ADDRESSES_CITY : addressChoiced.city,
                                         ADDRESSES_STATE : addressChoiced.state
                                         };
    
    [addresses addObject:customer_address];
    
    
    NSDictionary * customer = @{ ORDER_CUSTOMER_NAME : user.name,
                                 ORDER_CUSTOMER_CPF : user.personalID,
                                 USER_RG : user.personalRg,
                                 ORDER_CUSTOMER_EMAIL : user.email,
                                 CUSTOMER_CONTACTS : contacts,
                                 ADDRESSES : addresses
                                 };
    
    NSMutableArray * products = [NSMutableArray new];
    
    
    for( TrayCartObject * cart in itensOnCart){
        
        NSDictionary * productCart = @{
                                       TRANSACTION_PRODUCT_DESCRIPTION : cart.productName,
                                       TRANSACTION_PRODUCT_QUANTITY : cart.quantity,
                                       TRANSACTION_PRODUCT_PRICE_UNIT : [cart.price  stringByReplacingOccurrencesOfString:@"," withString:@"."],
                                       };
        
        [products addObject:productCart];
    }
    
    if(!shippingMethod){
        shippingMethod = [ShippingMethod new];
        shippingMethod.title = @"FRETE GRÁTIS";
        shippingMethod.value = @"0.00";
    }
    
    
    NSDictionary * transaction = @{
                                   TRANSACTION_SHIPPING_TYPE : shippingMethod.title,
                                   TRANSACTION_SHIPPING_PRICE : shippingMethod.value,
                                   TRANSACTION_SHIPPING_ORDER_NUMBER : orderID,
                                   TRANSACTION_SHIPPING_URL_NOTIFICATION : urlCallback,
                                   TRANSACTION_IP_NUMBER :[self getIPAddress]
                                   };
    NSDictionary* payment;
    
    if(paymentMethod.isCredit){
        
        CreditCard * creditCard = [TrayStorage getUserCreditCard];
        
        
        payment = @{ PAYMENT_METHOD_ID : paymentMethod.checkoutId,
                     PAYMENT_CARD_NAME : creditCard.ownerName,
                     PAYMENT_CARD_NUMBER : creditCard.cardNumber,
                     PAYMENT_CARD_SPLIT : creditCard.split,
                     PAYMENT_CARD_CVV : creditCard.cvv,
                     PAYMENT_CARD_EXPIRATE_MONTH : creditCard.monthExpirateDate,
                     PAYMENT_CARD_EXPIRATE_YEAR : creditCard.yearExpirateDate
                     };
    }else{
        payment = @{ PAYMENT_METHOD_ID : paymentMethod.checkoutId };
    }
    
    
    
    
    return @{
             TOKEN_ACCOUNT : [ServiceApiMapper Api].API_TOKEN_CHECKOUT,
             PRICE_SELLER : [NSString stringWithFormat:@"%.02f",[[TrayStorage getCartValue] floatValue]],
             CUSTOMER_ORDER : customer,
             TRANSACTION_PRODUCT : products,
             TRANSACTION : transaction,
             PAYMENT : payment
             };
    
}

+(BOOL) isSuccessTransation : (NSString*) apiResponse{
    NSError *error = nil;
    
    NSDictionary *json = [XMLReader dictionaryForXMLString:apiResponse error:&error];
    
    if(error){
        return NO;
    }
    
    
    NSString * RESPONSE = @"response";
    NSString * MESSAGE_RESPONSE = @"message_response";
    NSString * MESSAGE_RESPONSE_MESSAGE = @"message";
    NSString * ERROR = @"error";
    NSString * TEXT = @"text";
    
    id response = [self getValue:RESPONSE fromDictionary:json];
    
    if(!response){
        return NO;
    }
    
    id response_message = [self getValue:MESSAGE_RESPONSE fromDictionary:response];
    
    if(!response_message){
        return NO;
    }
    
    id message = [self getValue:MESSAGE_RESPONSE_MESSAGE fromDictionary:response_message];
    
    if(!message){
        return NO;
    }
    
    NSString* message_text = [self getValue:TEXT fromDictionary:message];
    
    if(message_text == nil ||  [message_text isEqualToString:ERROR]){
        return NO;
    }
    else{
        return YES;
    }
}

+(NSString*) getUrlRedirect : (NSString*) apiResponse{
    NSError *error = nil;
    
    NSDictionary *json = [XMLReader dictionaryForXMLString:apiResponse error:&error];
    
    if(error){
        return nil;
    }
    
    NSString * RESPONSE = @"response";
    NSString * DATA_RESPONSE = @"data_response";
    NSString * DATA_RESPONSE_TRANSATION = @"transaction";
    NSString * DATA_RESPONSE_TRANSATION_PAYMENT = @"payment";
    NSString * DATA_RESPONSE_TRANSATION_PAYMENT_URL = @"url_payment";
    NSString * TEXT = @"text";
    
    
    id response = [self getValue:RESPONSE fromDictionary:json];
    
    if(!response){
        return nil;
    }
    
    id dataResponse = [self getValue:DATA_RESPONSE fromDictionary:response];
    
    if(!dataResponse){
        return nil;
    }
    
    id dataResponseTransation = [self getValue:DATA_RESPONSE_TRANSATION fromDictionary:dataResponse];
    
    if(!dataResponseTransation){
        return nil;
    }
    
    id dataResponseTransationPayment = [self getValue:DATA_RESPONSE_TRANSATION_PAYMENT fromDictionary:dataResponseTransation];
    
    if(!dataResponseTransationPayment){
        return nil;
    }
    
    id dataResponseTransationPaymentUrl = [self getValue:DATA_RESPONSE_TRANSATION_PAYMENT_URL fromDictionary:dataResponseTransationPayment];
    
    if(!dataResponseTransationPaymentUrl){
        return nil;
    }
    
    NSString* url = [self getValue:TEXT fromDictionary:dataResponseTransationPaymentUrl];
    
    return url;
}

+(NSArray*) convertToLocalSplitSellers:(NSString*) apiResponse{
    
    NSError *error = nil;
    
    NSDictionary *json = [XMLReader dictionaryForXMLString:apiResponse error:&error];
    
    if(error){
        return nil;
    }
    
    id splitObject = [self getValue:@"seller_split" fromDictionary:json];
    
    if(!splitObject){
        return  nil;
    }
    
    id dataResponse = [self getValue:@"data_response" fromDictionary:splitObject];
    
    if(!dataResponse){
        return nil;
    }
    
    id splittings = [self getValue:@"splittings" fromDictionary:dataResponse];
    id splitting = [self getValue:@"splitting" fromDictionary:splittings];
    
    NSArray* payments = [NSArray new];
    
    if([splitting isKindOfClass:[NSArray class]]){
        
        for(id item in splitting){
            
            TrayPaymentsValue* value = [self getSplitValueFromDictionary:item];
            if(value){
                payments = [payments arrayByAddingObject:value];
            }
        }
    } else {
        TrayPaymentsValue* value = [self getSplitValueFromDictionary:splitting];
        if(value){
            payments = [payments arrayByAddingObject:value];
        }
    }
    return payments;
}

+(NSArray*) convertToLocalSimulatePayment:(NSString*) apiResponse{
    
    
    NSString* TRANSACTION = @"transaction";
    NSString* DATA_RESPONSE = @"data_response";
    NSString* PAYMENTS_METHODS = @"payment_methods";
    NSString* PAYMENT_METHOD = @"payment_method";

    NSError *error = nil;
    
    NSDictionary *json = [XMLReader dictionaryForXMLString:apiResponse error:&error];
    
    if(error){
        return nil;
    }
    
    id transaction = [self getValue:TRANSACTION fromDictionary:json];

    if(!transaction){
        return nil;
    }
    
    
    id dataResponse = [self getValue:DATA_RESPONSE fromDictionary:transaction];
    
    if(!dataResponse){
        return nil;
    }
    
    id paymentsMethods = [self getValue:PAYMENTS_METHODS fromDictionary:dataResponse];
    if(![paymentsMethods isKindOfClass:[NSDictionary class]]){
        return [self getPaymentsMethodsValuesFromDictionary:paymentsMethods];
    }
    else{
        paymentsMethods = [self getValue:PAYMENT_METHOD fromDictionary:paymentsMethods];
        for(id paymentMethod in paymentsMethods){
            
            NSArray * methods =  [self getPaymentsMethodsValuesFromDictionary:paymentMethod];
            if(methods){
                return methods;
            }
        }
    }
    return nil;
}

+(NSArray*) getPaymentsMethodsValuesFromDictionary:(id) dictionary{
    
    NSString* PAYMENT_METHOD_ID = @"payment_method_id";
    NSString* SPLITTINGS = @"splittings";
    NSString* SPLITTING = @"splitting";
    NSString* TEXT = @"text";
    
    id idPayment = [self getValue:PAYMENT_METHOD_ID fromDictionary:dictionary];

    if(!idPayment){
        return nil;
    }
    
    if([idPayment isKindOfClass:[NSDictionary class]]){
        idPayment = [self getValue:TEXT fromDictionary:idPayment];
    }
    
    if(![idPayment isEqualToString:[TrayStorage getPaymentsMethods].checkoutId]){
        return nil;
    }
    
    id splittings = [self getValue:SPLITTINGS fromDictionary:dictionary];
    
    if(!splittings){
        return  nil;
    }
    
    id splittingsObjects = [self getValue:SPLITTING fromDictionary:splittings];
    
    if(!splittingsObjects){
        return  nil;
    }
    
    NSMutableArray * items = [NSMutableArray new];
    
    if([splittingsObjects isKindOfClass:[NSDictionary class]]){
        
        TrayPaymentsValue * paymentValue =  [self getSplitValueFromDictionary:splittingsObjects];
        if(paymentValue){
            [items addObject:paymentValue];
        }
    }
    else{
        
        for(id splitObjet in splittingsObjects){
            
            TrayPaymentsValue * paymentValue =  [self getSplitValueFromDictionary:splitObjet];
            if(paymentValue){
                [items addObject:paymentValue];
            }
        }
    }
    return items;

}




+(TrayPaymentsValue*) getSplitValueFromDictionary:(id) dictionary{
    
    NSString * TEXT = @"text";
    NSString* SPLIT = @"split";
    NSString* VALUE_SPLIT = @"value_split";
    NSString* VALUE_TOTAL = @"value_transaction";
    
    if(!dictionary){
        return nil;
    }
    
    TrayPaymentsValue * paymentValue = [TrayPaymentsValue new];
    
    id split = [self getValue:SPLIT fromDictionary:dictionary];
    
    if(!split){
        return nil;
    }
    paymentValue.split = [self getValue:TEXT fromDictionary:split];
    
    id valueSplit = [self getValue:VALUE_SPLIT fromDictionary:dictionary];
    if(!valueSplit){
        return  nil;
    }
    
    paymentValue.valueSplit = [self getValue:TEXT fromDictionary:valueSplit];
    
    
    id valueTotal = [self getValue:VALUE_TOTAL fromDictionary:dictionary];
    if(!valueTotal){
        return  nil;
    }
    
    paymentValue.valueTotal = [self getValue:TEXT fromDictionary:valueTotal];
    
    return paymentValue;
}


#pragma ERRORS
+(NSString*) getErrorOnLoginUser:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    NSString* CAUSES = @"causes";
    
    id causes = [self getValue:CAUSES fromDictionary:json];
    
    if(!causes){
        return nil;
    }
    else{
        return @"Email e/ou senha inválidos";
    }

}
+(NSString*) getErrorOnCreateUser:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    
    NSString* CAUSES = @"causes";
    NSString* PERSONALID = @"cpf";
    
    id causes = [self getValue:CAUSES fromDictionary:json];
    
    if(!causes){
        return nil;
    }

    id objectCustomer = [self getValue:CUSTOMER fromDictionary:causes];
    
    if(!objectCustomer){
        return nil;
    }
    
    if([self getValue:EMAIL fromDictionary:objectCustomer]){
        return @"Esse email já está em uso";
    }
    else if([self getValue:PERSONALID fromDictionary:objectCustomer]){
        return @"CPF inválido ou já em uso";
    }
    return nil;
}
+(NSString*) getErrorUpdateUser:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    
    NSString* CAUSES = @"causes";
    NSString* PERSONALID = @"cpf";
    
    id causes = [self getValue:CAUSES fromDictionary:json];
    
    if(!causes){
        return nil;
    }
    
    id objectCustomer = [self getValue:CUSTOMER fromDictionary:causes];
    
    if(!objectCustomer){
        return nil;
    }
    
    if([self getValue:EMAIL fromDictionary:objectCustomer]){
        return @"Esse email já está em uso";
    }
    else if([self getValue:PERSONALID fromDictionary:objectCustomer]){
        return @"CPF inválido ou já em uso";
    }
    return nil;
}
+(NSString*) getErrorOnAddressCreate:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    
    NSString* CAUSES = @"causes";
    NSString* CUSTOMER_ADDRESS = @"CustomerAddress";
    
    id causes = [self getValue:CAUSES fromDictionary:json];
    
    if(!causes){
        return nil;
    }
    
    id objectCustomer = [self getValue:CUSTOMER_ADDRESS fromDictionary:causes];
    
    if(!objectCustomer){
        return nil;
    }else{
        return @"Há informações inválidas, por favor verifique e tente novamente";
    }
    
}
+(NSString*) getErrorOnInsertProduct:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    NSString* CAUSES = @"causes";
    id causes = [self getValue:CAUSES fromDictionary:json];
    
    if(!causes){
        return nil;
    }
    else{
        return @"Esse produto não possui essa quantidade em estoque";
    }
}


#pragma mark - Network specific methods
+(NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

@end

