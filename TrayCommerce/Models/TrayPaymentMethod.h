//
//  TrayPaymentMethod.h
//  TrayCommerce
//
//  Created by Diego P Navarro on 29/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentMethod.h"

@interface TrayPaymentMethod : PaymentMethod

@property (nonatomic, strong) NSString * checkoutId;
@property (nonatomic, strong) NSString * identifier;
@property (nonatomic, strong) NSString * maxPlots;
@property (nonatomic, strong) NSString * operatorName;
@property (nonatomic) BOOL isCredit;

@end
