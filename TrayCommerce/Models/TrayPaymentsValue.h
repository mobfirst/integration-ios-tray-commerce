//
//  TrayPaymentsValue.h
//  TrayCommerce
//
//  Created by Diego P Navarro on 30/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrayPaymentsValue : NSObject

@property (nonatomic, strong) NSString * split;
@property (nonatomic, strong) NSString * valueSplit;
@property (nonatomic, strong) NSString * valueTotal;
@end
