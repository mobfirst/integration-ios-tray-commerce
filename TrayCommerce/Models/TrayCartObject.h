//
//  TrayCartObject.h
//  TrayCommerce
//
//  Created by Diego P Navarro on 20/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrayCartObject : NSObject

@property (nonatomic, strong) NSString * token;
@property (nonatomic, strong) NSString * productId;
@property (nonatomic, strong) NSString * quantity;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * variant;
@property (nonatomic, strong) NSString * productName;
@property (nonatomic) bool virtualProduct;

@end
