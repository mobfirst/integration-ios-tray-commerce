//
//  UrlFormat+Tray.m
//  TrayCommerce
//
//  Created by Diego P Navarro on 20/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "UrlFormat+Tray.h"

@implementation UrlFormat(Tray)

+(NSString*) createUrlProductsCart:(NSArray*) itens{
    
    NSString* ids = @"";
    
    for(TrayCartObject* item in itens){
        ids = [NSString stringWithFormat:@"%@%@,",ids,item.productId];
    }
    
    if([ids length] > 0){
        ids =  [ids substringToIndex:[ids length]-1];
    }
    
    return ids;
}

+(NSString*) createUrlProductsVariantsCart:(NSArray*) itens{
    
    NSString* ids = @"";
    
    for(TrayCartObject* item in itens){
        if(![item.variant isEqualToString:@"0"]){
            ids = [NSString stringWithFormat:@"%@%@,",ids,item.variant];
        }
    }
    if([ids length] > 0){
        ids =  [ids substringToIndex:[ids length]-1];
    }
    
    return ids;
    
}

+(NSDictionary*) convertTrayCartObjectToUrl{
    
    NSMutableDictionary * dictionary = [NSMutableDictionary new];
    NSArray * productsCart = [TrayStorage getUserCarts];
    
    for(int count = 0 ; count < [productsCart count] ; count ++){
        
        TrayCartObject* object = [productsCart objectAtIndex:count];
        
        NSString* headerProductId = [NSString stringWithFormat:@"%@%@%d%@%@%@%@",
                                     HEADER_PRODUCTS, HEADER_ARRAY_OPEN, count, HEADER_ARRAY_CLOSE,HEADER_ARRAY_OPEN,HEADER_PRODUCT_ID,HEADER_ARRAY_CLOSE];
        NSString* headerProductPrice = [NSString stringWithFormat:@"%@%@%d%@%@%@%@",
                                        HEADER_PRODUCTS, HEADER_ARRAY_OPEN, count, HEADER_ARRAY_CLOSE,HEADER_ARRAY_OPEN,HEADER_PRICE,HEADER_ARRAY_CLOSE];
        
        NSString* headerProductQuantity = [NSString stringWithFormat:@"%@%@%d%@%@%@%@",
                                           HEADER_PRODUCTS, HEADER_ARRAY_OPEN, count, HEADER_ARRAY_CLOSE,HEADER_ARRAY_OPEN,HEADER_QUANTITY,HEADER_ARRAY_CLOSE];
        
        [dictionary addEntriesFromDictionary:@{headerProductId: object.productId}];
        [dictionary addEntriesFromDictionary:@{headerProductPrice: object.price}];
        [dictionary addEntriesFromDictionary:@{headerProductQuantity: object.quantity}];
    }
    return dictionary;
}

+(NSString*) formatTrayShippingToUrl:(NSString*) url andDictionary:(NSDictionary*) dictionary{
    
    url = [NSString stringWithFormat:@"%@&",url];
    
    for(NSString * key in [dictionary allKeys]){
        url = [NSString stringWithFormat:@"%@%@=%@&",url,key,[dictionary valueForKey:key]];
    }
    
    return [url substringToIndex:[url length]-1] ;
}

@end
