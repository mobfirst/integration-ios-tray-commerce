//
//  TrayUser.h
//  TrayCommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface TrayUser : User

@property (nonatomic, strong) NSString * token;
@property  (nonatomic, strong) NSArray * addressIds;

@end
