//
//  UrlFormat+Tray.h
//  TrayCommerce
//
//  Created by Diego P Navarro on 20/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UrlFormat.h"
#import "TrayCartObject.h"
#import "TrayStorage.h"

@interface UrlFormat(Tray)
+(NSString*) createUrlProductsCart:(NSArray*) itens;
+(NSString*) createUrlProductsVariantsCart:(NSArray*) itens;
+(NSDictionary*) convertTrayCartObjectToUrl;
+(NSString*) formatTrayShippingToUrl:(NSString*) url andDictionary:(NSDictionary*) dictionary;

@end
