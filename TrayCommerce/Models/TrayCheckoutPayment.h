//
//  TrayCheckoutPayment.h
//  TrayCommerce
//
//  Created by Diego P Navarro on 05/03/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrayCheckoutPayment : NSObject

@property (nonatomic, strong) NSString * trayCommerceId;
@property (nonatomic, strong) NSString * trayCheckoutId;
@property (nonatomic)  BOOL credit;

@end
