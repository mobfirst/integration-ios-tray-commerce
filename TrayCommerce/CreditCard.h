//
//  CreditCard.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/12/14.
//  Copyright (c) 2014 Vitrina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreditCard : NSObject

@property (nonatomic, strong) NSString * ownerName;
@property (nonatomic, strong) NSString * cardNumber;
@property (nonatomic, strong) NSString * monthExpirateDate;
@property (nonatomic, strong) NSString * yearExpirateDate;
@property (nonatomic, strong) NSString * cvv;
@property (nonatomic, strong) NSString * split;

@end
