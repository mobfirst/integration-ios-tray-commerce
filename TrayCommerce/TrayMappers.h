//
//  TrayMappers.h
//  TrayCommerce
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceApiMapper.h"
#import "Store.h"
#import "Product.h"
#import "Address.h"
#import "ProductCategory.h"
#import "Option.h"
#import "Variation.h"
#import "Store.h"

#import "Product.h"
#import "Address.h"
#import "Email.h"
#import "Phones.h"
#import "ProductCategory.h"
#import "Option.h"
#import "Variation.h"
#import "TrayUser.h"

#import "TrayConstants.h"
#import "TrayCartObject.h"
#import "ProductCart.h"
#import "Cart.h"
#import "TrayPaymentMethod.h"
#import "XMLReader.h"

#import "TrayPaymentsValue.h"

#define CONFIGURATION_TO_GET @[@"sem_estoque",@"sem_imagem",@"sem_preco",@"sem_peso",@"preco_prod_indisponivel"]

@interface TrayMappers : NSObject

#pragma Store Information
+(NSString*) getTrayRefreshToken:(NSString*) apiResponse;
+(NSString*) getTrayAccessToken:(NSString*) apiResponse;
+(BOOL) getStoreCustomSettings:(id) dictionary;
+(NSDictionary*) mapperStoreConfiguration:(NSString*) key;
+(BOOL) storageStoreConfiguration:(NSString*) response;

#pragma Category
+(NSArray*) convertToLocalCategories:(NSString*) apiResponse;

#pragma Products
+(NSArray*) convertToLocalProducts:(NSString*) apiResponse;
+(Product*) convertToLocalProduct:(NSString*) apiResponse;
+(NSArray*) convertToLocalProductVariants:(NSString*) apiResponse;

#pragma User
+(User*) confirmUserCreate: (User*) user onResponse:(NSString*) apiResponse;
+(User*) convertToLocalUser:(NSString*) apiResponse;
+(NSString*) convertToUserId:(NSString*) apiResponse;

#pragma Addresses
+(NSArray*) convertToLocalCustomerAddresses:(NSString*) apiResponse;
+(NSString*) confirmAddressCreate:(NSString*) apiResponse;

#pragma Carts
+(NSArray*) convertTolocalCart:(NSString*) apiResponse;
+(Cart*) convertTolocalCartItems:(NSArray*) cartObjects products:(NSArray*) products variations:(NSArray*) variations;

#pragma Payments Methods
+(NSArray*) convertToLocalPaymentsMethods:(NSString*) apiResponse;

#pragma Shipping Methods
+(NSArray*) convertToLocalShippingMethods:(NSString*) apiResponse;

#pragma ORDER
+(NSDictionary*) createOrderToTrayCommerce;
+(NSString*) confirmCreateOrder:(NSString*) apiResponse;
+(NSString*) getUrlCallback:(NSString*) apiResponse;
+(NSDictionary*) createOrderToTrayCheckoutWithUrl:(NSString*) urlCallback andOrderId:(NSString*) orderID;
+(NSDictionary*) getTrayCheckoutSimulateObject;
+(NSArray*) convertToLocalSimulatePayment:(NSString*) apiResponse;
+(NSArray*) convertToLocalSplitSellers:(NSString*) apiResponse;
+(BOOL) isSuccessTransation : (NSString*) apiResponse;
+(NSString*) getUrlRedirect : (NSString*) apiResponse;


#pragma ERRORS
+(NSString*) getErrorOnLoginUser:(NSString*) apiResponse;
+(NSString*) getErrorOnCreateUser:(NSString*) apiResponse;
+(NSString*) getErrorUpdateUser:(NSString*) apiResponse;
+(NSString*) getErrorOnAddressCreate:(NSString*) apiResponse;
+(NSString*) getErrorOnInsertProduct:(NSString*) apiResponse;

@end
