//
//  NSString+HTML.h
//  NuvemShop
//
//  Created by Diego P Navarro on 20/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HTML)

- (NSString *)decodeHTMLCharacterEntities;
- (NSString *)encodeHTMLCharacterEntities;


@end
