//
//  ServiceApiMapper.h
//  TrayCommerce
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceApiModel.h"
#import "TrayConstants.h"
#import "UrlFormat+Tray.h"
#import <PlataformCommunication/PlataformCommunication.h>
#import "TrayMappers.h"
#import "ServiceStorageUser.h"
#import "TrayStorage.h"
#import "Cart.h"
#import "TrayUser.h"
#import "ProductCartToUpdate.h"

@interface ServiceApiMapper : NSObject<ServiceApiModel>

@property (strong,nonatomic) NSString* API_CODE;
@property (strong,nonatomic) NSString* API_URL;
@property (strong,nonatomic) NSString* API_TOKEN;
@property (strong,nonatomic) NSString* MAX_INSTALLMENTS;
@property (strong,nonatomic) NSString* API_TOKEN_CHECKOUT;
@property (strong,nonatomic) NSString* STORE_CONFIG;
@property (nonatomic) int attempt;

+(ServiceApiMapper*) Api;
-(void) getTrayCheckoutTransationValuesWithBlock:(void (^) (NSArray*, NSError*)) block;
-(int) cpfRequired;
-(int) rgRequired;

@end
