//
//  ServiceApiMapper.m
//  TrayCommerce
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#define ERROR_CONVERTION @"ERROR_CONVERTION"
#

#define ATTEMPT_LIMIT 2

#import "ServiceApiMapper.h"
#import "ServiceApiLogger.h"
#import "APIConnection.h"
#import <PlataformModels/ServiceStorageStore.h>

#define DEBUG_MOBFIRST_API NO

@implementation ServiceApiMapper

static ServiceApiMapper * mapper;

+(ServiceApiMapper*) Api{
    if(!mapper){
      mapper = [ServiceApiMapper new];
      mapper.attempt = 0;
    }
    return mapper;
}

#pragma ERROR MAKER
-(NSError*) getErrorWithTitle:(NSString*) error{
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:error forKey:NSLocalizedDescriptionKey];
    return  [NSError errorWithDomain:error code:ERROR_CONVERTION_CODE userInfo:details];
}

#pragma Store Information
-(void) getStoreInformationWithBlock:(void (^)(NSObject*,NSError*))block{
    [[APIConnection getInstance] getConnectionAuthentication:HEADER_MOBFIRST_CLIENT_ID_VALUE secret:MOBFIRST_HEADER_CLIENT_SECRET_VALUE block:^(NSString* token , NSError* error){
        
        if(error){
            [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:@"" method:@"getStoreInformationWithBlock/auth" withError:error isDebug:DEBUG_MOBFIRST_API andResponse:@""];
            block(nil,error);
        }
        else{
            
            [[APIConnection getInstance] getMobFirstStoreInformation:token id:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE block:^(NSObject *object, id customSettings, NSError *error2) {
                if(error2){
                    [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:@"" method:@"getStoreInformationWithBlock/storeInformation" withError:error isDebug:DEBUG_MOBFIRST_API andResponse:@""];
                    block(nil,error2);
                }else{
                    [TrayMappers getStoreCustomSettings:customSettings];
                    Store* store = (Store*) object;
                    block(store,nil);
                }
            }];
        }
    }];
}


#pragma TOKEN AND AUTH
-(void) getTokenTokenFromTrayCommerceWithBlock: (void (^) (BOOL, NSError*)) block{
    
    NSDictionary* params =@{
                            HEADER_MOBFIRST_CLIENT_ID:HEADER_MOBFIRST_CLIENT_ID_VALUE,
                            HEADER_MOBFIRST_CLIENT_SECRET : MOBFIRST_HEADER_CLIENT_SECRET_VALUE,
                            HEADER_MOBFIRST_CLIENT_APP_ID: HEADER_MOBFIRST_CLIENT_APP_ID_VALUE,
                            HEADER_MOBFIRST_CLIENT_STORE_CODE : [ServiceApiMapper Api].API_CODE
                            };
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypePost urlString:[NSString stringWithFormat:@"%@/%@", MOBFIRST_REFRESH_BASE_URL,@"refresh_token"] path: nil parameters:params changeSerializer:YES  andResponseBlock:^(NSString * response, NSDictionary* headers, NSError * error) {
        
        if(error){
            
            [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:@"getTokenTokenFromTrayCommerceWithBlock" withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            block(NO,error);
        }else {
            
            NSString* refreshToken = [TrayMappers getTrayRefreshToken:response];
            
            if(!refreshToken){
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:@"getTokenTokenFromTrayCommerceWithBlock" withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                block(NO,[self getErrorWithTitle:ERROR_CONVERTION]);
            }
            else{
                [ServiceApiMapper Api].API_TOKEN = refreshToken;
                [self getConfigurationStore:^(BOOL success, NSError * error) {
                    block(YES,nil);
                }];
            }
        }
        
    }] connectionStart];
}



#pragma Configuration Store
-(void) getConfigurationStore: (void (^) (BOOL, NSError*)) block{
    
    
    /*HEADER TOKEN*/
    NSDictionary* dictionary ;
    
    if([ServiceApiMapper Api].API_TOKEN!= nil){
        dictionary =@{
                      HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN,
                      REGISTRY_PARAM_NAME: REGISTRY_PARAM_VALUE
                      };
    }
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl: [ServiceApiMapper Api].API_URL andComplemention:@"configuration"];
    
    PlataformCommunication* commmunication =   [PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:url path: nil andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:@"getConfigurationStore" withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                
                block(NO,error);
            }
            else{
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(NO,error1);
                    }
                    else{
                        [self getConfigurationStore:block];
                    }
                }];
            }
        }else{
            [ServiceApiMapper Api].attempt  = 0 ;
            block([TrayMappers storageStoreConfiguration:response],nil);
        }
    }];
    
    [commmunication connectionStart];
    
}

#pragma Products Category
-(void) getCategoriesWithBlock:(void (^)(NSArray*,NSError*))block{
    
    /*HEADER TOKEN*/
    NSDictionary* dictionary ;
    
    if([ServiceApiMapper Api].API_TOKEN!= nil){
        dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    }
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl: [ServiceApiMapper Api].API_URL andComplemention:@"categories/tree/0"];
    
    PlataformCommunication* commmunication =   [PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:url path: nil andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:@"getCategoriesWithBlock" withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                
                block(nil,error);
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getCategoriesWithBlock:block];
                    }
                }];
            }
        }else{
            [ServiceApiMapper Api].attempt  = 0 ;
            NSArray* array  = [TrayMappers convertToLocalCategories:response];
            if(!array){
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:@"getCategoriesWithBlock" withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                block(nil,[self getErrorWithTitle:ERROR_CONVERTION]);
            }else{
                block(array,nil);
            }
        }
    }];
    
    [commmunication connectionStart];
}
-(void) getCategoryImage:(NSString *)id withBlock:(void (^)(NSObject *, NSError *))block{
    
    if([id isEqualToString:@"-1000"]){
        [self getAllProductsOnPage:1 andQuantity:1 withBlock:^(NSArray * products, NSError * error) {
            
            if(error){
                block(nil,error);
            }else{
                if([products count] > 0){
                    Product* product = [products objectAtIndex:0];
                    if(product.images && [product.images count] > 0){
                        block([product.images  objectAtIndex:0], nil);
                    }
                    else{
                        block(nil,nil);
                    }
                }else{
                    block(nil,nil);
                }
            }
        }];
        
    }else{
        [self getProductsByCategoryId:id onPage:1 andQuantity:1 withBlock:^(NSArray * products, NSError * error) {
            
            if(error){
                block(nil,error);
            }else{
                if([products count] > 0){
                    Product* product = [products objectAtIndex:0];
                    if(product.images && [product.images count] > 0){
                        block([product.images  objectAtIndex:0], nil);
                    }
                    else{
                        block(nil,nil);
                    }
                }else{
                    block(nil,nil);
                }
            }
        }];
    }
    
    
}

#pragma Products

-(NSMutableDictionary*) getProductDefaultParameters:(int) page andQuantity:(int) qnty {

    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithDictionary:
                                       @{
                                         HEADER_TOKEN: [ServiceApiMapper Api].API_TOKEN,
                                         HEADER_LIMIT: [NSString stringWithFormat:@"%d", qnty],
                                         HEADER_PAGE: [NSString stringWithFormat:@"%d", page]}];
    
    
    for(NSString* key in CONFIGURATION_TO_GET) {
        id value = [TrayMappers mapperStoreConfiguration:key];
        
        if([key isEqualToString:@"sem_estoque"]) {
            if(value) {
                
                NSString* valueFromKey = [value objectForKey:key];
                if([valueFromKey isEqualToString:@"1"]) {
                    [dictionary addEntriesFromDictionary:@{@"stock": @">=0"}];
                }
                else {
                    [dictionary addEntriesFromDictionary:@{@"stock": @">=1"}];
                }
            }
            else {
                [dictionary addEntriesFromDictionary:@{@"stock": @">=1"}];
            }
        }
//        else if([key isEqualToString:@"sem_imagem"]) {
//            if(value) {
//                
//                NSString* valueFromKey = [value objectForKey:key];
//                if([valueFromKey isEqualToString:@"1"]) {
//                    [dictionary addEntriesFromDictionary:@{@"image": @"0"}];
//                }
//                else {
//                    [dictionary addEntriesFromDictionary:@{@"image": @"1"}];
//                }
//            }
//            else {
//                [dictionary addEntriesFromDictionary:@{@"image": @"1"}];
//            }
//
//        }
        else if([key isEqualToString:@"sem_preco"]) {
            if(value) {
                
                NSString* valueFromKey = [value objectForKey:key];
                if([valueFromKey isEqualToString:@"1"]) {
                    [dictionary addEntriesFromDictionary:@{@"price": @">=0"}];
                }
                else {
                    [dictionary addEntriesFromDictionary:@{@"price": @">=0.01"}];
                }
            }
            else {
                [dictionary addEntriesFromDictionary:@{@"price": @">=0"}];
            }
        }
        else if([key isEqualToString:@"sem_peso"]) {
            if(value) {
                
                NSString* valueFromKey = [value objectForKey:key];
                if([valueFromKey isEqualToString:@"1"]) {
                    [dictionary addEntriesFromDictionary:@{@"weight": @">=0"}];
                }
                else {
                    [dictionary addEntriesFromDictionary:@{@"weight": @">=1"}];
                }
            }
            else {
                [dictionary addEntriesFromDictionary:@{@"weight": @">=0"}];
            }
        }
        else if([key isEqualToString:@"preco_prod_indisponivel"]) {
            if(value) {
                
                NSString* valueFromKey = [value objectForKey:key];
                if([valueFromKey isEqualToString:@"1"]) {
                    [dictionary addEntriesFromDictionary:@{@"available": @"0"}];
                }
                else {
                    [dictionary addEntriesFromDictionary:@{@"available": @"1"}];
                }
            }
            else {
                [dictionary addEntriesFromDictionary:@{@"available": @"1"}];
            }
        }
    }
    
    return dictionary;

}



-(void) getAllProductsOnPage:(int) page andQuantity:(int) quantity withBlock:(void (^)(NSArray*,NSError*))block{
    
    /*HEADERS*/
    NSDictionary* dictionary = [self getProductDefaultParameters:page andQuantity:quantity];
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl: [ServiceApiMapper Api].API_URL andComplemention:@"products"];
    
    PlataformCommunication* communication =   [PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:url path: nil andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            
            [ServiceApiMapper Api].attempt ++ ;
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:@"getAllProductsOnPage" withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                block(nil,error);
            }
            else{
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getAllProductsOnPage:page andQuantity:quantity withBlock:block];
                    }
                    
                }];
            }
        }else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSArray* array  = [TrayMappers convertToLocalProducts:response];
            if(!array){
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:@"getAllProductsOnPage" withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                block(nil,[self getErrorWithTitle:ERROR_CONVERTION]);
            }else{
                block(array,nil);
            }
        }
    }];
    
    [communication connectionStart];
    
}
-(void) getProductsByCategoryId:(NSString*) categoryId onPage:(int) page andQuantity:(int) quantity withBlock:(void (^)(NSArray*,NSError*))block{
   
    /*HEADERS*/
    NSMutableDictionary* dictionary = [self getProductDefaultParameters:page andQuantity:quantity];
    [dictionary addEntriesFromDictionary:  @{HEADER_CATEGORY_ID: categoryId}];
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl: [ServiceApiMapper Api].API_URL andComplemention:@"products"];
    
    PlataformCommunication* communication =   [PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:url path: nil andResponseBlock:^(NSString * response, NSDictionary* headers,NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                NSString* method = [NSString stringWithFormat:@"getProductsByCategoryId/%@",categoryId];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                
                block(nil,error);
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getProductsByCategoryId:categoryId onPage:page andQuantity:quantity withBlock:block];
                    }
                    
                }];
            }
        }else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            NSArray* array  = [TrayMappers convertToLocalProducts:response];
            if(!array){
                NSString* method = [NSString stringWithFormat:@"getProductsByCategoryId/%@",categoryId];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
    
                block(nil,[self getErrorWithTitle:ERROR_CONVERTION]);
            }else{
                block(array,nil);
            }
        }
        
    }];
    
    [communication connectionStart];
    
}
-(void) getProductsBySearch:(NSString*) search onPage:(int) page andQuantity:(int) quantity withBlock:(void (^)(NSArray*,NSError*))block{
    
    search = [search stringByReplacingOccurrencesOfString:@" "withString:@"+"];
    search = [[NSString alloc] initWithData:[search dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES] encoding:NSUTF8StringEncoding];
    
    
    /*HEADERS*/
    NSMutableDictionary* dictionary = [self getProductDefaultParameters:page andQuantity:quantity];
    [dictionary addEntriesFromDictionary:  @{HEADER_NAME: [NSString stringWithFormat:@"%@",search]}];
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl: [ServiceApiMapper Api].API_URL andComplemention:@"products/search/1"];
    
    PlataformCommunication* communication =   [PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:url path: nil andResponseBlock:^(NSString * response, NSDictionary* headers,NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                
                NSString* method = [NSString stringWithFormat:@"getProductsBySearch/%@",search];
                
                   [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                block(nil,error);
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getProductsBySearch:search onPage:page andQuantity:quantity withBlock:block];
                    }
                    
                }];
            }
        }else{
            [ServiceApiMapper Api].attempt  = 0 ;
            NSArray* array  = [TrayMappers convertToLocalProducts:response];
            if(!array){
                NSString* method = [NSString stringWithFormat:@"getProductsBySearch/%@",search];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                block(nil,[self getErrorWithTitle:ERROR_CONVERTION]);
            }else{
                block(array,nil);
            }
        }
    }];
    
    [communication connectionStart];
}
-(void) getProductById:(NSString*) id withBlock:(void (^)(NSObject*,NSError*))block{
    
    /*HEADER TOKEN*/
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl: [ServiceApiMapper Api].API_URL andComplemention:[NSString stringWithFormat:@"products/%@",id]];
    
    PlataformCommunication* communication =   [PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:url path: nil andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                NSString* method = [NSString stringWithFormat:@"getProductById/%@",id];
                
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            
                block(nil,error);
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getProductById:id withBlock:block];
                    }
                    
                }];
            }
        }else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            
            Product* object =  [TrayMappers convertToLocalProduct:response];
            if(!object){
                NSString* method = [NSString stringWithFormat:@"getProductVariant/%@",id];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                block(nil,[self getErrorWithTitle:ERROR_CONVERTION]);
            }else{
                if(object.option){
                    
                    [self getProductVariant:object.id withBlock:^(NSArray *variants, NSError * error) {
                        
                        if(error){
                            block(nil,error);
                        }else{
                            object.option.childrens = variants;
                            block(object,nil);
                        }
                    }];
                }
                else{
                    block(object,nil);
                }
            }
        }
    }];
    
    [communication connectionStart];
    
}
-(void) getProductVariant:(NSString*) id withBlock:(void (^)(NSArray*,NSError*))block{
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN,
                                HEADER_PRODUCT_ID:id};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl: [ServiceApiMapper Api].API_URL andComplemention:@"variants"];
    
    PlataformCommunication* communication =   [PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:url path: nil andResponseBlock:^(NSString * response, NSDictionary* headers,NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                NSString* method = [NSString stringWithFormat:@"getProductVariant/%@",id];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                block(nil,error);
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getProductVariant:id withBlock:block];
                    }
                    
                }];
            }
        }else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSArray* array  = [TrayMappers convertToLocalProductVariants:response];
            if(!array){
                NSString* method = [NSString stringWithFormat:@"getProductVariant/%@",id];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                block(nil,[self getErrorWithTitle:ERROR_CONVERTION]);
            }else{
                block(array,nil);
            }
        }
    }];
    
    [communication connectionStart];
}

#pragma User
-(void) createUser:(User*) user withBlock:(void (^) (BOOL, NSError*)) block{
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:@"customers"];
    
    NSDictionary* params =@{
                            USER_NAME:user.name,
                            USER_EMAIL: user.email,
                            USER_CPF : user.personalID,
                            USER_PHONE : user.phone,
                            USER_PASSWORD : user.password,
                            USER_RG : user.personalRg
                            };
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypePost urlString: url path:nil parameters:params andResponseBlock:^(NSString *response,NSDictionary* headers, NSError *error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){

                NSString* errorString = [TrayMappers getErrorOnCreateUser: response];
                NSError* errorLogger = errorString ? [self getErrorWithTitle:errorString] : error;
                
                block(NO,errorLogger);
                
                NSString* method = [NSString stringWithFormat:@"createUser/%@",params];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:errorLogger isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(NO,error1);
                    }
                    else{
                        [self createUser:user withBlock:block];
                    }
                    
                }];
            }
        }
        else{
            [ServiceApiMapper Api].attempt  = 0 ;
            
            User* userCreate = [TrayMappers confirmUserCreate:user onResponse:response];
            
            if(!userCreate){
                block(NO,[self getErrorWithTitle:ERROR_CONVERTION]);
                NSString* method = [NSString stringWithFormat:@"createUser/%@",params];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                [self getUser:userCreate withBlock:block];
            }
        }
    }] connectionStart];
    
}
-(void) loginUser:(User*) user withBlock:(void (^) (BOOL, NSError*)) block{
    
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:@"customers/login"];
    
    NSDictionary* params =@{
                            USER_EMAIL: user.email,
                            USER_PASSWORD : user.password
                            };
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypePost urlString: url path:nil parameters:params andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                NSString* errorString = [TrayMappers getErrorOnLoginUser: response];
                NSError* errorLogger = errorString ? [self getErrorWithTitle:errorString] : error;
                
                block(NO,errorLogger);

                NSString* method = [NSString stringWithFormat:@"loginUser/%@",params];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:errorLogger isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(NO,error1);
                    }
                    else{
                        [self loginUser:user withBlock:block];
                    }
                    
                }];
            }
        }
        else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSString* userId = [TrayMappers convertToUserId:response];
            
            if(!userId){
                block(NO,[self getErrorWithTitle:ERROR_CONVERTION]);
                NSString* method = [NSString stringWithFormat:@"loginUser/%@",params];
                
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                user.id = userId;

                [self getUser: user withBlock:block];
            }
        }
        
    }] connectionStart];
    
    
}
-(void) updateUser:(User*) user withBlock:(void (^) (BOOL, NSError*)) block{
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:[NSString stringWithFormat:@"customers/%@",user.id]];
    
    NSDictionary* params =@{
                            USER_NAME:user.name,
                            USER_EMAIL: user.email,
                            USER_PHONE : user.phone,
                            USER_RG : user.personalRg
                            };
    
    NSDictionary * customerParams = @{CUSTOMER : params};
    
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypePut urlString: url path:nil parameters:customerParams andResponseBlock:^(NSString *response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                NSString* errorString = [TrayMappers getErrorUpdateUser: response];
                NSError* errorLogger = errorString ? [self getErrorWithTitle:errorString] : error;
                
                block(NO,errorLogger);
                
                NSString* method = [NSString stringWithFormat:@"updateUser/%@",params];
                
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:errorLogger isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(NO,error1);
                    }
                    else{
                        [self updateUser:user withBlock:block];
                    }
                    
                }];
            }
        }
        else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            block([ServiceStorageUser saveUser:user],nil);
        }
        
    }] connectionStart];
    
}
-(void) getUser:(User*) user withBlock:(void (^) (BOOL, NSError*)) block{
    
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:[NSString stringWithFormat:@"customers/%@",user.id]];
    
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:url path:nil andResponseBlock: ^(NSString *response,NSDictionary* headers, NSError * error){
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                block(NO,error);
                NSString* method = [NSString stringWithFormat:@"getUser -  Url:%@",url];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(NO,error1);
                    }
                    else{
                        [self getUser:user withBlock:block];
                    }
                    
                }];
            }
        }
        else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            User * userInformation = [TrayMappers convertToLocalUser:response];
            
            if(!userInformation){
                
                NSString* method = [NSString stringWithFormat:@"getUser -  Url:%@",url];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                
                
                block(NO,[self getErrorWithTitle:ERROR_CONVERTION]);
            }
            else{

                block([ServiceStorageUser saveUser:userInformation],nil);
            }
        }
        
    }] connectionStart];
}


#pragma Address
-(void) createAddress:(Address*) address withBlock:(void (^) (BOOL, NSError*)) block{
    
    TrayUser * user = (TrayUser*)[ServiceStorageUser getUserLogged];
    
    if(!user){
        block(NO,[self getErrorWithTitle:CONVERT_ERROR_GENERIC]);
        return;
    }
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:@"customer_addresses"];
    
    NSDictionary* params =@{
                            ADDRESSES_CUSTOMER_ID: user.id,
                            ADDRESSES_LINE_1: address.addressLine1,
                            ADDRESSES_LINE_2 : address.addressLine2,
                            ADDRESSES_NUMBER : address.number,
                            ADDRESSES_NEIGHBORHOOD : address.neighborhood,
                            ADDRESSES_CITY : address.city,
                            ADDRESSES_STATE : address.state,
                            ADDRESSES_POSTAL_CODE : address.postalCode,
                            ADDRESSES_COUNTRY : ADDRESSES_COUNTRY_CODE
                            };
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypePost urlString: url path:nil parameters:params andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                NSString* errorString = [TrayMappers getErrorOnAddressCreate: response];
                NSError* errorLogger = errorString ? [self getErrorWithTitle:errorString] : error;
                
                block(NO,errorLogger);
                
                NSString* method = [NSString stringWithFormat:@"createAddress/%@",params];
                
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:errorLogger isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(NO,error1);
                    }
                    else{
                        [self createAddress:address withBlock:block];
                    }
                    
                }];
            }
        }else {
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSString * addressId = [TrayMappers confirmAddressCreate:response];
            
            if(addressId){
                NSMutableArray * addresses = [NSMutableArray new];
                [addresses addObject: addressId];
                user.addressIds = addresses;
                block([ServiceStorageUser saveUser:user],nil);
            }
            else{
                block(NO,[self getErrorWithTitle:CONVERT_ERROR_GENERIC]);
                NSString* method = [NSString stringWithFormat:@"createAddress/%@",params];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:CONVERT_ERROR_GENERIC]  isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
        }
        
    }] connectionStart];
}
-(void) updateAddress:(Address*) address withBlock:(void (^) (BOOL, NSError*)) block{
    
    /**
     * TRAY COMMERCE API ALREADY UPDATE THE ADDRESS INFORMATION WHEN A NEW IS CREATE
     * */
    
    [self createAddress:address withBlock:block];
    
}
-(void) deleteAddress:(Address*) address withBlock:(void (^) (BOOL, NSError*)) block{
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:[NSString stringWithFormat: @"customer_addresses/%@",address.id]];
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeDelete urlString: url path:nil andResponseBlock:^(NSString * response, NSDictionary* headers,NSError * error) {
        
        if(error){
            
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                block(NO,error);
                NSString* method = [NSString stringWithFormat:@"deleteAddress/%@",address.id];
                
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(NO,error1);
                    }
                    else{
                        [self deleteAddress:address withBlock:block];
                    }
                    
                }];
            }
            
            
        }else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            block(YES,nil);
        }
    }] connectionStart];
    
}
-(void) getUserAddressesWithBlock:(void (^) (NSArray* itens, NSError* error)) block{
    
    TrayUser * user =(TrayUser*) [ServiceStorageUser getUserLogged];
    
    if(user == nil || user.addressIds == nil || [user.addressIds count] == 0){
        block([NSArray new],nil);
        NSString* method = [NSString stringWithFormat:@"getUserAddressesWithBlock"];
        
        [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:@"USER NULL"]  isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:@"USER NULL"];
        return;
    }
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    /***
     * TRAY COMMERCE API RESPONSE ONLY ONE CUSTOMER A ADDRESS
     * GET THE FIRST ITEM ON LIST
     * */
    
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:[NSString stringWithFormat: @"customer_addresses/%@",[user.addressIds objectAtIndex:0]]];
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString: url path:nil andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                block(nil,error);
                NSString* method = [NSString stringWithFormat:@"getUserAddressesWithBlock/%@",url];
                
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getUserAddressesWithBlock:block];
                    }
                    
                }];
            }
        }else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSArray * addresses = [TrayMappers convertToLocalCustomerAddresses:response];
            
            if(!addresses){
                block(nil ,[self getErrorWithTitle:ERROR_CONVERTION]);
                NSString* method = [NSString stringWithFormat:@"getUserAddressesWithBlock/%@",url];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                block(addresses,nil);
            }
        }
    }] connectionStart];
}
-(void) getStatesToCreateAddressWithBlock:(void (^) (NSArray*, NSError*)) block{
    
    NSMutableArray * itens = [NSMutableArray new];
    
    [itens addObject:@"AC"];
    [itens addObject:@"AL"];
    [itens addObject:@"AP"];
    [itens addObject:@"AM"];
    [itens addObject:@"BA"];
    [itens addObject:@"CE"];
    [itens addObject:@"DF"];
    [itens addObject:@"ES"];
    [itens addObject:@"GO"];
    [itens addObject:@"MA"];
    [itens addObject:@"MT"];
    [itens addObject:@"MS"];
    [itens addObject:@"MG"];
    [itens addObject:@"PA"];
    [itens addObject:@"PB"];
    [itens addObject:@"PR"];
    [itens addObject:@"PE"];
    [itens addObject:@"PI"];
    [itens addObject:@"RJ"];
    [itens addObject:@"RN"];
    [itens addObject:@"RS"];
    [itens addObject:@"RO"];
    [itens addObject:@"RR"];
    [itens addObject:@"SC"];
    [itens addObject:@"SP"];
    [itens addObject:@"SE"];
    [itens addObject:@"TO"];
    
    block(itens,nil);
}
-(void) choiceShippingAddress: (Address*) address  withBlock:(void (^) (BOOL, NSError*)) block{
    block([TrayStorage saveChoicedAddress:address],nil);
}

#pragma Cart
-(void) getCartWithBlock:(void (^) (NSObject*, NSError*)) block{
    
    TrayUser * user = (TrayUser*)[ServiceStorageUser getUserLogged];
    
    if(user == nil){
        block(nil,[self getErrorWithTitle:CONVERT_ERROR_GENERIC]);
        return;
    }
    
    NSDictionary* dictionary =@{
                                HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN,
                                HEADER_SESSION_ID:user.token,
                                };
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:@"cart"];
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString: url path:nil andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                block(nil,error);
                NSString* method = [NSString stringWithFormat:@"getCartWithBlock/%@",url];
                
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getCartWithBlock:block];
                    }
                    
                }];
            }
        }
        else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSArray* objects = [TrayMappers convertTolocalCart:response];
            
            if(!objects){
                NSString* method = [NSString stringWithFormat:@"getCartWithBlock/%@",url];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                block(nil ,[self getErrorWithTitle:ERROR_CONVERTION]);
            }
            else{
                if([objects count] == 0){
                    block(nil ,nil);
                }else{
                    [self getProductsFromCart:objects withBlock:block];
                }
            }
        }
        
    }] connectionStart];
}
-(void) getProductsFromCart:(NSArray*) objects withBlock:(void (^) (NSObject*, NSError*)) block{
    
    
    NSDictionary* dictionary =@{
                                HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN,
                                HEADER_ID:[UrlFormat createUrlProductsCart:objects]
                                };
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:@"products"];
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString: url path:nil andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                block(nil,error);
                NSString* method = [NSString stringWithFormat:@"getProductsFromCart/"];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getCartWithBlock:block];
                    }
                    
                }];
            }
        }
        else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSArray* array  = [TrayMappers convertToLocalProducts:response];
            if(!array){
                block(nil,[self getErrorWithTitle:ERROR_CONVERTION]);
                NSString* method = [NSString stringWithFormat:@"getProductsFromCart/"];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                [self getVariantsFromCarts:objects andProducts:array withBlock:block];
            }
        }
        
    }] connectionStart];
}
-(void) getVariantsFromCarts:(NSArray*) carts andProducts:(NSArray*) products withBlock:(void (^) (NSObject*, NSError*)) block{
    
    
    NSDictionary* dictionary =@{
                                HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN,
                                HEADER_ID:[UrlFormat createUrlProductsVariantsCart:carts]
                                };
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:@"variants"];
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString: url path:nil andResponseBlock:^(NSString * response, NSDictionary* headers,NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                block(nil,error);
                NSString* method = [NSString stringWithFormat:@"getVariantsFromCarts/"];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getVariantsFromCarts:carts andProducts:products withBlock:block];
                    }
                    
                }];
            }
        }
        else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSArray* variants = [TrayMappers convertToLocalProductVariants:response];
            
            if(!variants){
                block(nil,[self getErrorWithTitle:ERROR_CONVERTION]);
            }
            else{
                Cart* cart = [TrayMappers convertTolocalCartItems:carts products:products variations:variants];
                
                if(!cart){
                    block(nil,[self getErrorWithTitle:ERROR_CONVERTION]);
                    NSString* method = [NSString stringWithFormat:@"getVariantsFromCarts/"];
                    
                    [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                }
                else{
                    block(cart,nil);
                }
            }
        }
    }] connectionStart];
    
    
}
-(void) insertProductsOnCart:(NSArray*) products withBlock:(void (^) (BOOL, NSError*)) block{
    [self insertProductToCart:products index:0 withBlock:block];
}
-(void) insertProductToCart:(NSArray*) products index:(int) index withBlock:(void (^) (BOOL, NSError*)) block{

    if(index == [products count]){
        block(YES ,nil);
        return;
    }
    
    TrayUser * user = (TrayUser*) [ServiceStorageUser getUserLogged];
    
    if(user == nil){
        block(NO,[self getErrorWithTitle:CONVERT_ERROR_GENERIC]);
        NSString* method = [NSString stringWithFormat:@"insertProductToCart/"];
        
        [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:@"USER NULL"] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:@"USER NULL"];
        return;
    }
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    
    ProductCart* product = [products objectAtIndex:index];
    
    NSDictionary* cartObject =@{
                                CART_SESSION_ID: user.token,
                                CART_PRODUCT_ID: product.id,
                                CART_QUANTITY : [NSString stringWithFormat:@"%lu",(long)product.quantity],
                                CART_PRICE : product.specialPrice ? product.specialPrice : product.price,
                                CART_VARIANT_ID :product.option == nil ? @"0" : ((Variation*)[product.option.childrens objectAtIndex:0]).id
                                };
    
    NSDictionary * params =  @{CART_CART : cartObject};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:@"cart"];
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypePost urlString:url path:nil parameters:params andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                NSString* errorString = [TrayMappers getErrorOnInsertProduct: response];
                NSError* errorLogger = errorString ? [self getErrorWithTitle:errorString] : error;
                
                block(YES,errorLogger);
                
                NSString* method = [NSString stringWithFormat:@"insertProductToCart/%@",params];
                
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:errorLogger isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(NO,error1);
                    }
                    else{
                        [self insertProductToCart:products  index:index withBlock:block];
                    }
                    
                }];
            }
        }else{
            [ServiceApiMapper Api].attempt  = 0 ;
            
            [self insertProductToCart:products  index:index +1 withBlock:block];
        }
    }] connectionStart];
}
-(void) updateProductsOnCart:(NSArray*) products withBlock:(void (^) (BOOL, NSError*)) block{
    [self updateProductOnCart:products atIndex:0 andBlock:block];
}
-(void) updateProductOnCart:(NSArray*) products atIndex: (int) index andBlock:(void (^) (BOOL, NSError*)) block{
    
    if(index == [products count]){
         block(YES ,nil);
        return;
    }
    
    ProductCartToUpdate* product = [products objectAtIndex:index];
    
    if(product.product.quantity < product.newQuantity){
        
        product.product.quantity = product.newQuantity - product.product.quantity;
        
        [self insertProductsOnCart:[NSArray arrayWithObjects:product.product, nil] withBlock: ^(BOOL success, NSError * error) {
        
            if(error || !success){
                block(success,error);
            }
            else{
                [self updateProductOnCart:products atIndex: index + 1 andBlock:block];
            }
        }];
    }
    else{
        [self deleteProductOnCart:product.product withBlock:^(BOOL success, NSError * error) {
            
            if(error || !success){
                block(NO,error);
            }
            else{
                product.product.quantity = product.newQuantity;
                [self insertProductsOnCart:[NSArray arrayWithObjects:product.product, nil] withBlock: ^(BOOL success, NSError * error) {
                    
                    if(error || !success){
                        block(NO,error);
                    }
                    else{
                        [self updateProductOnCart:products atIndex: index + 1 andBlock:block];
                    }
                }];
            }
        }];
    }
}
-(void) deleteProductOnCart:(ProductCart*) product withBlock:(void (^) (BOOL, NSError*)) block{
    
    /*
     * TRAY COMMERCE API UPDATE THE PRODUCT, IF QUANTITY IS EQUAL ZERO, THEN THE PRODUCT IS DELETED
     */
    
    product.quantity = 0;
    [self insertProductsOnCart:[NSArray arrayWithObjects:product, nil] withBlock:block];
}
-(void) confirmCheckoutWithCart:(Cart*) cart withBlock:(void (^) (NSObject*, NSError*)) block{
    block([TrayStorage saveCart:cart] ? @ "SUCCESS" : @"ERROR",nil);
}

#pragma Payments
-(void) getPaymentMethodsWithBlock:(void (^) (NSArray*, NSError*)) block{
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:@"payments/methods/1/"];
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:url path:nil andResponseBlock:^(NSString *response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                block(nil,error);
                NSString* method = [NSString stringWithFormat:@"getPaymentMethodsWithBlock/"];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getPaymentMethodsWithBlock:block];
                    }
                    
                }];
            }
        }
        else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSArray * itensPayments = [TrayMappers convertToLocalPaymentsMethods:response];
            if(!itensPayments){
                block(nil, [self getErrorWithTitle:CONVERT_ERROR_GENERIC]);
                NSString* method = [NSString stringWithFormat:@"getPaymentMethodsWithBlock/"];
                
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                block(itensPayments,nil);
            }
        }
        
    }] connectionStart];
}
-(void) insertPaymentMethod:(PaymentMethod*) paymentMethod WithBlock:(void (^) (BOOL, NSError*)) block{
    block([TrayStorage savePaymentsMethods:paymentMethod],nil);
}

#pragma Shipping
-(void) getShippingMethodsWithBlock:(void (^) (NSArray*, NSError*)) block{
    
    Address* address = [TrayStorage getChoicedAddress];
    
    NSDictionary* dictionary =@{ HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN,
                                 HEADER_ZIP_CODE : address.postalCode };
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:@"shippings/cotation"];
    url = [UrlFormat formatTrayShippingToUrl:url andDictionary:[UrlFormat convertTrayCartObjectToUrl]];
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString: url path:nil andResponseBlock:^(NSString *response, NSDictionary* headers,NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                block(nil,error);
                NSString* method = [NSString stringWithFormat:@"getShippingMethodsWithBlock/"];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(nil,error1);
                    }
                    else{
                        [self getShippingMethodsWithBlock:block];
                    }
                    
                }];
            }
        }
        else{
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSArray* shippingMethods = [TrayMappers convertToLocalShippingMethods:response];
            
            if(!shippingMethods){
                block(nil, [self getErrorWithTitle:CONVERT_ERROR_GENERIC]);
                NSString* method = [NSString stringWithFormat:@"getShippingMethodsWithBlock/"];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                block(shippingMethods,nil);
            }
        }
        
    }] connectionStart];
    
}
-(void) insertShippingMethod:(ShippingMethod*) method WithBlock:(void (^) (BOOL, NSError*)) block{
    block([TrayStorage saveShippingMethods:method],nil);
}


#pragma Order
-(void) finnishOrderWithBlock:(void (^) (BOOL, NSString*,NSError*)) block{
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention:@"orders/create"];
    
    NSDictionary * params = [TrayMappers createOrderToTrayCommerce];
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypePost urlString:url path:nil parameters:params changeSerializer:YES andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                block(NO,nil,error);
                NSString* method = [NSString stringWithFormat:@"finnishOrderWithBlock/"];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(NO,nil,error1);
                    }
                    else{
                        [self finnishOrderWithBlock:block];
                    }
                    
                }];
            }
        }
        else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSString* orderID = [TrayMappers confirmCreateOrder: response];
            
            if(!orderID){
                block(NO,nil, [self getErrorWithTitle:CONVERT_ERROR_GENERIC]);
                NSString* method = [NSString stringWithFormat:@"finnishOrderWithBlock/"];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                if(![[TrayStorage getShippingMethods] paymentRequired]){
                
                    block(YES,nil, nil);
                    
                }else{
                
                    [self getTrayCommerceOrderUrlCallBack:orderID andBlock:block];
                }
                
                
                
            }
        }
        
    }] connectionStart];
}
-(void) getTrayCommerceOrderUrlCallBack:(NSString*) orderID andBlock: (void (^) (BOOL, NSString*,NSError*)) block{
    
    
    NSDictionary* dictionary =@{HEADER_TOKEN:[ServiceApiMapper Api].API_TOKEN};
    
    NSString* url = [UrlFormat insertParametersQuery:dictionary OnBaseUrl:[ServiceApiMapper Api].API_URL andComplemention: [NSString stringWithFormat:@"orders/%@",orderID]];
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString: url path:nil andResponseBlock:^(NSString *response,NSDictionary* headers, NSError * error) {
        
        if(error){
            [ServiceApiMapper Api].attempt ++ ;
            
            if(ATTEMPT_LIMIT <= [ServiceApiMapper Api].attempt){
                block(NO,nil,error);
                NSString* method = [NSString stringWithFormat:@"getTrayCommerceOrderUrlCallBack/%@",orderID];
                
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }
            else{
                
                [self getTokenTokenFromTrayCommerceWithBlock:^(BOOL success, NSError * error1) {
                    
                    if(error1){
                        block(NO,nil,error1);
                    }
                    else{
                        [self getTrayCommerceOrderUrlCallBack:orderID andBlock:block];
                    }
                    
                }];
            }
        }
        else{
            
            [ServiceApiMapper Api].attempt  = 0 ;
            
            NSString* urlCallback = [TrayMappers getUrlCallback:response];
            
            if(!urlCallback){
                block(NO,nil, [self getErrorWithTitle:CONVERT_ERROR_GENERIC]);
                NSString* method = [NSString stringWithFormat:@"getTrayCommerceOrderUrlCallBack/%@",orderID];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
            }else{
            
                [self createOrderOnTrayCheckoutWithCallBackUrl:urlCallback orderID: orderID andBlock:block];
              
            }
        }
    }] connectionStart];
    
}
-(void) createOrderOnTrayCheckoutWithCallBackUrl:(NSString*) callbackUrl orderID:(NSString*) orderID andBlock: (void (^) (BOOL, NSString*,NSError*)) block{
    
    
    NSString* url = [UrlFormat insertParametersQuery:nil OnBaseUrl:TRAY_CHECKOUT_BASE_URL andComplemention:@"transactions/pay_complete"];
    
    NSDictionary * orderTrayCheckout = [TrayMappers createOrderToTrayCheckoutWithUrl: callbackUrl andOrderId:orderID];
    NSDictionary * headers = @{HEADER_RESELLER_TOKEN:HEADER_RESELLER_TOKEN_VALUE};
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypePost urlString: url path:nil headers:headers parameters:orderTrayCheckout andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        if(error){
            block(NO,nil, [self getErrorWithTitle:CONVERT_ERROR_GENERIC]);
            NSString* method = [NSString stringWithFormat:@"createOrderOnTrayCheckoutWithCallBackUrl/%@",orderTrayCheckout];
            
            [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
        }else{
            
            BOOL success = [TrayMappers isSuccessTransation:response];
            
            if(!success){
                block(NO,nil, [self getErrorWithTitle:CONVERT_ERROR_GENERIC]);
                NSString* method = [NSString stringWithFormat:@"createOrderOnTrayCheckoutWithCallBackUrl/%@",orderTrayCheckout];
                [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:[self getErrorWithTitle:ERROR_CONVERTION] isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
                
            }else{
                NSString * url = [TrayMappers getUrlRedirect:response];
                block(YES,[url length] == 0 ? nil : url, nil);
            }
        }
    }] connectionStart];
}
-(void) getTrayCheckoutTransationValuesWithBlock:(void (^) (NSArray*, NSError*)) block{
    
    
    NSString* url = [UrlFormat insertParametersQuery:nil OnBaseUrl:TRAY_CHECKOUT_BASE_URL_CURRENT andComplemention:@"seller_splits/simulate_split"];
    
    NSDictionary * simulateOrderTrayCheckout = [TrayMappers getTrayCheckoutSimulateObject];
    NSDictionary * headers = @{HEADER_RESELLER_TOKEN:HEADER_RESELLER_TOKEN_VALUE};
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypePost urlString: url path:nil headers:headers parameters:simulateOrderTrayCheckout andResponseBlock:^(NSString * response,NSDictionary* headers, NSError * error) {
        
        if(error){
            block(nil,error);
            NSString* method = [NSString stringWithFormat:@"getTrayCheckoutTransationValuesWithBlock/%@",simulateOrderTrayCheckout];
            [ServiceApiLogger senderErrorAPIonClient:HEADER_MOBFIRST_CLIENT_ID_VALUE clientId:HEADER_MOBFIRST_CLIENT_APP_ID_VALUE clientStoreName:[ServiceStorageStore getDefaultStore].name method:method withError:error isDebug:[ServiceStorageStore getDefaultStore].debug andResponse:response];
        }
        else{
            block([TrayMappers convertToLocalSplitSellers:response],nil);
        }
        
    }] connectionStart];
}

-(int) rgRequired {
    
    id value = [TrayMappers mapperStoreConfiguration:@"rg_obrigatorio"];
    if(value) {
        
        if([[value objectForKey:@"rg_obrigatorio"] isEqualToString:@"0"]) {
            return 0;
        }
        else if([[value objectForKey:@"rg_obrigatorio"] isEqualToString:@"1"]) {
            return 1;
        }
        else {
            return -1;
        }
    }
    
    return -1;
}

-(int) cpfRequired {
    
    id value = [TrayMappers mapperStoreConfiguration:@"cpf"];
    if(value) {
        
        if([[value objectForKey:@"cpf"] isEqualToString:@"0"]) {
            return 0;
        }
        else if([[value objectForKey:@"cpf"] isEqualToString:@"1"]) {
            return 1;
        }
        else {
            return -1;
        }
    }
    
    return -1;
}

-(int) cnpjRequired {
    
    id value = [TrayMappers mapperStoreConfiguration:@"cnpj"];
    if(value) {
        
        if([[value objectForKey:@"cnpj"] isEqualToString:@"0"]) {
            return 0;
        }
        else if([[value objectForKey:@"cnpj"] isEqualToString:@"1"]) {
            return 1;
        }
        else if([[value objectForKey:@"cnpj"] isEqualToString:@"3"]) {
            return 3;
        }
        else {
            return -1;
        }
    }
    
    return -1;
}

@end
