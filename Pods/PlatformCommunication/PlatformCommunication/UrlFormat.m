//
//  UrlFormat.m
//  PlatformCommunication
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "UrlFormat.h"

@implementation UrlFormat

+(NSString*) insertParametersQuery:(NSDictionary*) dictionary OnBaseUrl:(NSString*) url andComplemention:(NSString*) complemention{
    
    url = [NSString stringWithFormat:@"%@%@",url,complemention];
    url = [NSString stringWithFormat:@"%@?",url];
    if(dictionary){
        for (NSString* key in dictionary) {
            NSString* value = [dictionary objectForKey:key];
            url = [NSString stringWithFormat:@"%@%@=%@&",url,[key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ] ,[value stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        }
    }
    return [url substringToIndex:[url length]-1];
}


@end
