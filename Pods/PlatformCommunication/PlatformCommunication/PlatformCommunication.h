//
//  PlatformCommunication.h
//  PlatformCommunication
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

// Que dia util.

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "UrlFormat.h"
#import "Constants.h"

@interface PlatformCommunication : NSObject

@property (nonatomic) ConnectionType connectionType;
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSString * path;
@property (nonatomic, strong) NSDictionary * parameters;
@property (nonatomic, strong) NSDictionary * headers;
@property (nonatomic) BOOL  changeSerializer;
@property (nonatomic, strong) void(^responseBlock)(NSString *, NSDictionary* headers, NSError *);

+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path andResponseBlock:(void (^)(NSString*, NSDictionary* headers,NSError*))block;

+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path headers:(NSDictionary*) headers andResponseBlock:(void (^)(NSString*,NSDictionary* headers,NSError*))block;

+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path headers:(NSDictionary*) headers parameters:(NSDictionary*) parameters andResponseBlock:(void (^)(NSString*,NSDictionary* headers,NSError*))block;

+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path parameters:(NSDictionary*) parameters andResponseBlock:(void (^)(NSString*,NSDictionary* headers,NSError*))block;

+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path parameters:(NSDictionary*) parameters changeSerializer:(BOOL) changeSerializer andResponseBlock:(void (^)(NSString*,NSDictionary* headers,NSError*))block;

+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path headers:(NSDictionary*) headers parameters:(NSDictionary*) parameters changeSerializer:(BOOL) changeSerializer andResponseBlock:(void (^)(NSString*,NSDictionary* headers,NSError*))block;

-(void) connectionStart;

@end
