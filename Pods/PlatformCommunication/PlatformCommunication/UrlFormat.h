//
//  UrlFormat.h
//  PlatformCommunication
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UrlFormat : NSObject

+(NSString*) insertParametersQuery:(NSDictionary*) dictionary OnBaseUrl:(NSString*) url andComplemention:(NSString*) complemention;


@end
