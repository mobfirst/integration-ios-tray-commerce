//
//  Constants.h
//  PlatformCommunication
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#ifndef PlatformCommunication_Constants_h
#define PlatformCommunication_Constants_h

typedef enum {
    ConnectionTypeGet,
    ConnectionTypePut,
    ConnectionTypePost,
    ConnectionTypeDelete,
} ConnectionType;

#endif
