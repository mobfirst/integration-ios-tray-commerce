//
//  PlatformCommunication.m
//  PlatformCommunication
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "PlatformCommunication.h"

@implementation PlatformCommunication



+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path andResponseBlock:(void (^)(NSString*,NSDictionary*,NSError*))block
{
    return  [PlatformCommunication initWithConnectionType:connectionType urlString:url path:path parameters:nil andResponseBlock:block];
}

+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path headers:(NSDictionary*) headers andResponseBlock:(void (^)(NSString*,NSDictionary*,NSError*))block{
    return [PlatformCommunication initWithConnectionType:connectionType urlString:url path:path headers:headers parameters:nil andResponseBlock:block];
}


+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path headers:(NSDictionary*) headers parameters:(NSDictionary*) parameters andResponseBlock:(void (^)(NSString*,NSDictionary*,NSError*))block
{
    PlatformCommunication * connection = [PlatformCommunication new];
    connection.connectionType = connectionType;
    connection.url = url;
    connection.path =path;
    connection.responseBlock = block;
    connection.parameters = parameters;
    connection.headers = headers;
    return connection;
}

+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path parameters:(NSDictionary*) parameters andResponseBlock:(void (^)(NSString*,NSDictionary*,NSError*))block
{
    PlatformCommunication * connection = [PlatformCommunication new];
    connection.connectionType = connectionType;
    connection.url = url;
    connection.path = path;
    connection.responseBlock = block;
    connection.parameters = parameters;
    
    return connection;
}

+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path parameters:(NSDictionary*) parameters changeSerializer:(BOOL) changeSerializer andResponseBlock:(void (^)(NSString*,NSDictionary*,NSError*))block
{
    PlatformCommunication * connection = [PlatformCommunication new];
    connection.connectionType = connectionType;
    connection.url = url;
    connection.path = path;
    connection.responseBlock = block;
    connection.parameters = parameters;
    connection.changeSerializer = changeSerializer;
    return connection;
}

+(PlatformCommunication *) initWithConnectionType:(ConnectionType) connectionType urlString:(NSString*) url path:(NSString*) path headers:(NSDictionary*) headers parameters:(NSDictionary*) parameters changeSerializer:(BOOL) changeSerializer andResponseBlock:(void (^)(NSString*,NSDictionary* headers,NSError*))block{
    
    PlatformCommunication * connection = [PlatformCommunication new];
    connection.connectionType = connectionType;
    connection.url = url;
    connection.path = path;
    connection.responseBlock = block;
    connection.parameters = parameters;
    connection.changeSerializer = changeSerializer;
    connection.headers = headers;
    return connection;
    
}


-(void) connectionStart{

    switch (self.connectionType) {
        case ConnectionTypeDelete:
            [self sendRequestDelete];
            break;
        case ConnectionTypePost:
            [self sendRequestPost];
            break;
        case ConnectionTypePut:
            [self sendRequestPut];
            break;
        case ConnectionTypeGet:
            [self sendRequestGet];
            break;
        default:
            break;
    }
}


-(AFHTTPRequestOperationManager*) setAFHTTPRequestOperationManager
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
    
    if(self.changeSerializer){
       serializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    }

    manager.responseSerializer = serializer;
    if(self.headers){
        NSArray* keys = [self.headers allKeys];
        for (NSString*  key in keys) {
            [manager.requestSerializer setValue:[self.headers valueForKey:key] forHTTPHeaderField:key];
        }
    }
    return manager;
}


-(void) sendRequestGet{
    [[self setAFHTTPRequestOperationManager] GET:self.url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
      
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,error);
        }
    }];
}

-(void) sendRequestPut{
    
    [[self setAFHTTPRequestOperationManager]  PUT:self.url  parameters:self.parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,error);
        }
    }];
}

-(void) sendRequestPost{
    
    [[self setAFHTTPRequestOperationManager]  POST:self.url  parameters:self.parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,error);
        }
    }];
}

-(void) sendRequestDelete{
    [[self setAFHTTPRequestOperationManager]  POST:self.url  parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSDictionary *dictionary = nil;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)operation.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            dictionary = [httpResponse allHeaderFields];
        }
        
        if(self.responseBlock){
            self.responseBlock(operation.responseString,dictionary,error);
        }
    }];
}




@end
