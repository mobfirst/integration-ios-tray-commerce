//
//  ServiceStorageStore.m
//  PlatformModels
//
//  Created by Diego P Navarro on 15/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "ServiceStorageStore.h"
#import "Constants.h"
#import "Address.h"
#import "Email.h"
#import "Phones.h"


#define STORAGE_SOCIAL_NETWORKS_FACEBOOK @ "STORAGE_SOCIAL_NETWORKS_FACEBOOK"
#define STORAGE_SOCIAL_NETWORKS_INSTAGRAM @ "STORAGE_SOCIAL_NETWORKS_INSTAGRAM"
#define STORAGE_SOCIAL_NETWORKS_INSTAGRAM_TAG @ "STORAGE_SOCIAL_NETWORKS_INSTAGRAM_TAG"
#define STORAGE_SOCIAL_NETWORKS_TWITTER @ "STORAGE_SOCIAL_NETWORKS_TWITTER"
#define STORAGE_SOCIAL_NETWORKS_YOUTUBE @ "STORAGE_SOCIAL_NETWORKS_YOUTUBE"

#define STORAGE_TITLE @"STORAGE_TITLE"
#define STORAGE_LOGO @"STORAGE_LOGO"
#define STORAGE_COLOR @"STORAGE_COLOR"
#define STORAGE_DEBUG @ "STORAGE_DEBUG"
#define STORAGE_BULD_VERSION @ "STORAGE_BULD_VERSION"

#define STORAGE_BANNERS @"STORAGE_BANNERS"

#define STORAGE_CONTACT_INFORMATION_ADDRESSES @ "STORAGE_CONTACT_INFORMATION_ADDRESSES" 

#define STORAGE_CONTACT_INFORMATION_ADDRESSES_ID @ "STORAGE_CONTACT_INFORMATION_ADDRESSES_ID" 
#define STORAGE_CONTACT_INFORMATION_ADDRESSES_LINE_1 @ "STORAGE_CONTACT_INFORMATION_ADDRESSES_LINE_1" 
#define STORAGE_CONTACT_INFORMATION_ADDRESSES_LINE_2 @ "STORAGE_CONTACT_INFORMATION_ADDRESSES_LINE_2" 
#define STORAGE_CONTACT_INFORMATION_ADDRESSES_NEIGHBORHOOD @ "STORAGE_CONTACT_INFORMATION_ADDRESSES_NEIGHBORHOOD" 
#define STORAGE_CONTACT_INFORMATION_ADDRESSES_NUMBER @ "STORAGE_CONTACT_INFORMATION_ADDRESSES_NUMBER" 
#define STORAGE_CONTACT_INFORMATION_ADDRESSES_POSTAL_CODE @ "STORAGE_CONTACT_INFORMATION_ADDRESSES_POSTAL_CODE" 
#define STORAGE_CONTACT_INFORMATION_ADDRESSES_CITY @ "STORAGE_CONTACT_INFORMATION_ADDRESSES_CITY" 
#define STORAGE_CONTACT_INFORMATION_ADDRESSES_STATE @ "STORAGE_CONTACT_INFORMATION_ADDRESSES_STATE" 
#define STORAGE_CONTACT_INFORMATION_ADDRESSES_COUNTRY @ "STORAGE_CONTACT_INFORMATION_ADDRESSES_COUNTRY" 

#define STORAGE_CONTACT_INFORMATION_PHONES @ "STORAGE_CONTACT_INFORMATION_PHONES" 
#define STORAGE_CONTACT_INFORMATION_PHONES_TITLE @ "STORAGE_CONTACT_INFORMATION_PHONES_TITLE" 
#define STORAGE_CONTACT_INFORMATION_PHONES_AREA_CODE @ "STORAGE_CONTACT_INFORMATION_PHONES_AREA_CODE" 
#define STORAGE_CONTACT_INFORMATION_PHONES_COUNTRY_CODE @ "STORAGE_CONTACT_INFORMATION_PHONES_COUNTRY_CODE" 
#define STORAGE_CONTACT_INFORMATION_PHONES_NUMBER @ "STORAGE_CONTACT_INFORMATION_PHONES_NUMBER" 

#define STORAGE_CONTACT_INFORMATION_EMAILS @ "STORAGE_CONTACT_INFORMATION_EMAILS" 

#define STORAGE_CONTACT_INFORMATION_EMAILS_TITLE @ "STORAGE_CONTACT_INFORMATION_EMAILS_TITLE" 
#define STORAGE_CONTACT_INFORMATION_EMAILS_EMAIL @ "STORAGE_CONTACT_INFORMATION_EMAILS_EMAIL" 

@implementation ServiceStorageStore

+(BOOL) saveDefaultStore:(Store*) store{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject: store.facebook forKey:STORAGE_SOCIAL_NETWORKS_FACEBOOK];
    [defaults setObject: store.instagram forKey:STORAGE_SOCIAL_NETWORKS_INSTAGRAM];
    [defaults setObject: store.instagramTag forKey:STORAGE_SOCIAL_NETWORKS_INSTAGRAM_TAG];
    
    [defaults setObject: store.twitter forKey:STORAGE_SOCIAL_NETWORKS_TWITTER];
    [defaults setObject: store.youtube forKey:STORAGE_SOCIAL_NETWORKS_YOUTUBE];
    
    [defaults setObject: store.name forKey:STORAGE_TITLE];
    [defaults setObject: store.logo forKey:STORAGE_LOGO];
    [defaults setObject: store.color forKey:STORAGE_COLOR];
    [defaults setBool:store.debug forKey:STORAGE_DEBUG];
    [defaults setObject: store.model  == BUSINESS ? @"BUSINESS" : @"ENTERPRISE" forKey:STORAGE_BULD_VERSION];
    
    [defaults setObject:store.banners forKey:STORAGE_BANNERS];
    [defaults setObject:[self getDictionaryFromAddresses:store.addresses] forKey:STORAGE_CONTACT_INFORMATION_ADDRESSES];
    [defaults setObject:[self getDictionaryFromEmails:store.emails] forKey:STORAGE_CONTACT_INFORMATION_EMAILS];
    [defaults setObject:[self getDicitionaryFromPhones:store.phones] forKey:STORAGE_CONTACT_INFORMATION_PHONES];
    
    return [defaults synchronize];

}


+(id) getDictionaryFromAddresses:(NSArray*) addresses{
    
    NSMutableArray* dictionary = [NSMutableArray new];
  
    for(Address * address in addresses){
        NSMutableDictionary* dictionaryAddress = [NSMutableDictionary new];
        [dictionaryAddress setValue:address.id forKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_ID];
        [dictionaryAddress setValue:address.addressLine1 forKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_LINE_1];
        [dictionaryAddress setValue:address.addressLine2 forKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_LINE_2];
        [dictionaryAddress setValue:address.number forKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_NUMBER];
        [dictionaryAddress setValue:address.postalCode forKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_POSTAL_CODE];
        [dictionaryAddress setValue:address.neighborhood forKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_NEIGHBORHOOD];
        [dictionaryAddress setValue:address.city forKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_CITY];
        [dictionaryAddress setValue:address.state forKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_STATE];
        [dictionaryAddress setValue:address.country forKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_COUNTRY];
        
        [dictionary addObject:dictionaryAddress];
    }
    
    return dictionary;
}



+(id) getDictionaryFromEmails:(NSArray*) emails{
    
    NSMutableArray* dictionary = [NSMutableArray new];
    for(Email * email in emails){
        NSMutableDictionary* dictionaryEmail = [NSMutableDictionary new];

        [dictionaryEmail setValue:email.title forKey:STORAGE_CONTACT_INFORMATION_EMAILS_TITLE];
        [dictionaryEmail setValue:email.email forKey:STORAGE_CONTACT_INFORMATION_EMAILS_EMAIL];
        [dictionary addObject:dictionaryEmail];
    }
    
    return dictionary;
}


+(id) getDicitionaryFromPhones:(NSArray*) phones{
    
    
    NSMutableArray* dictionary = [NSMutableArray new];
    
    for(Phones * phone in phones){
        NSMutableDictionary* dictionaryPhone = [NSMutableDictionary new];
        
        [dictionaryPhone setValue:phone.title forKey:STORAGE_CONTACT_INFORMATION_PHONES_TITLE];
        [dictionaryPhone setValue:phone.countryCode forKey:STORAGE_CONTACT_INFORMATION_PHONES_COUNTRY_CODE];
        [dictionaryPhone setValue:phone.areaCode forKey:STORAGE_CONTACT_INFORMATION_PHONES_AREA_CODE];
        [dictionaryPhone setValue:phone.number forKey:STORAGE_CONTACT_INFORMATION_PHONES_NUMBER];
        
        [dictionary addObject:dictionaryPhone];
    }
    return dictionary;
}

+(Store*) getDefaultStore{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    Store* store = [Store new];
    
    store.facebook = [defaults objectForKey:STORAGE_SOCIAL_NETWORKS_FACEBOOK];
    store.instagram = [defaults objectForKey:STORAGE_SOCIAL_NETWORKS_INSTAGRAM];
    store.instagramTag = [defaults objectForKey:STORAGE_SOCIAL_NETWORKS_INSTAGRAM_TAG];
    store.twitter = [defaults objectForKey:STORAGE_SOCIAL_NETWORKS_TWITTER];
    store.youtube = [defaults objectForKey:STORAGE_SOCIAL_NETWORKS_YOUTUBE];
    
    store.name = [defaults objectForKey:STORAGE_TITLE];
    store.logo = [defaults objectForKey:STORAGE_LOGO];
    store.color = [defaults objectForKey:STORAGE_COLOR];
    store.model = [[defaults objectForKey:STORAGE_BULD_VERSION] isEqualToString:@"BUSINESS"] ? BUSINESS : ENTERPRISE;
    store.debug = [defaults boolForKey:STORAGE_DEBUG];
    store.banners = [self getBannersListFromDictionary:[defaults objectForKey:STORAGE_BANNERS]];
    store.addresses = [self getAddressesArrayListFromDictionary:[defaults objectForKey:STORAGE_CONTACT_INFORMATION_ADDRESSES]];
    store.emails = [self getEmailsArrayListFromDictionary:[defaults objectForKey:STORAGE_CONTACT_INFORMATION_EMAILS]];
    store.phones = [self getPhonesArrayLisFromDictionary:[defaults objectForKey:STORAGE_CONTACT_INFORMATION_PHONES]];
    
    return store;
    
}

+(NSArray*) getBannersListFromDictionary:(id) dictionary{
    
     NSMutableArray* banners = [NSMutableArray new];
    
    for(NSString* banner in dictionary){
        [banners addObject:banner];
    }
    
    return banners;
}


+(NSArray*) getAddressesArrayListFromDictionary:(id) dictionary{

    NSMutableArray* addresses = [NSMutableArray new];
    
    for(id dicAddress in dictionary){
        
        Address* address = [Address new];
        
        address.id  = [dicAddress objectForKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_ID];
        address.addressLine1 = [dicAddress objectForKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_LINE_1];
        address.addressLine2 = [dicAddress objectForKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_LINE_2];
        address.number = [dicAddress objectForKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_NUMBER];
        address.postalCode = [dicAddress objectForKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_POSTAL_CODE];
        address.neighborhood = [dicAddress objectForKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_NEIGHBORHOOD];
        address.city = [dicAddress objectForKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_CITY];
        address.state = [dicAddress objectForKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_STATE];
        address.country = [dicAddress objectForKey:STORAGE_CONTACT_INFORMATION_ADDRESSES_COUNTRY];
        
        [addresses addObject:address];
    }
    return addresses;
}


+(NSArray*) getEmailsArrayListFromDictionary:(id) dictionary{

    NSMutableArray* emails = [NSMutableArray new];
    
    for(id dicEmails in dictionary){
        
        Email * email = [Email new];
        email.title = [dicEmails objectForKey:STORAGE_CONTACT_INFORMATION_EMAILS_TITLE];
        email.email = [dicEmails objectForKey:STORAGE_CONTACT_INFORMATION_EMAILS_EMAIL];
        
        [emails addObject:email];
    }
    return emails;
}

+(NSArray*) getPhonesArrayLisFromDictionary:(id) dictionary{
    
    NSMutableArray * phones = [NSMutableArray new];
    
    for(id dicPhones in dictionary){
        
        Phones * phone = [Phones new];
        phone.title = [dicPhones objectForKey:STORAGE_CONTACT_INFORMATION_PHONES_TITLE];
        phone.countryCode =  [dicPhones objectForKey:STORAGE_CONTACT_INFORMATION_PHONES_COUNTRY_CODE];
        phone.areaCode =  [dicPhones objectForKey:STORAGE_CONTACT_INFORMATION_PHONES_AREA_CODE];
        phone.number =  [dicPhones objectForKey:STORAGE_CONTACT_INFORMATION_PHONES_NUMBER];
        
        [phones addObject:phone];
    }
    
    return phones;
}

@end
