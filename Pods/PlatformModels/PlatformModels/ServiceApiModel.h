//
//  ServiceApiModel.h
//  PlatformModels
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "User.h"
#import "Address.h"
#import "Product.h"
#import "PaymentMethod.h"
#import "ShippingMethod.h"
#import "Cart.h"

@protocol ServiceApiModel <NSObject>

@required
#pragma Store Information
-(void) getStoreInformationWithBlock:(void (^)(NSObject*,NSError*))block;

#pragma Products Category
-(void) getCategoriesWithBlock:(void (^)(NSArray*,NSError*))block;
-(void) getCategoryImage:(NSString*) id withBlock:(void (^) (NSObject*, NSError*)) block;

#pragma Products
-(void) getAllProductsOnPage:(int) page andQuantity:(int) quantity withBlock:(void (^)(NSArray*,NSError*))block;
-(void) getProductsByCategoryId:(NSString*) categoryId onPage:(int) page andQuantity:(int) quantity withBlock:(void (^)(NSArray*,NSError*))block;
-(void) getProductsBySearch:(NSString*) search onPage:(int) page andQuantity:(int) quantity withBlock:(void (^)(NSArray*,NSError*))block;
-(void) getProductById:(NSString*) id withBlock:(void (^)(NSObject*,NSError*))block;

#pragma User
-(void) createUser:(User*) user withBlock:(void (^) (BOOL, NSError*)) block;
-(void) loginUser:(User*) user withBlock:(void (^) (BOOL, NSError*)) block;
-(void) updateUser:(User*) user withBlock:(void (^) (BOOL, NSError*)) block;
-(void) getUser:(User*) user withBlock:(void (^) (BOOL, NSError*)) block;

#pragma Address
-(void) createAddress:(Address*) address withBlock:(void (^) (BOOL, NSError*)) block;
-(void) updateAddress:(Address*) address withBlock:(void (^) (BOOL, NSError*)) block;
-(void) deleteAddress:(Address*) address withBlock:(void (^) (BOOL, NSError*)) block;
-(void) getUserAddressesWithBlock:(void (^) (NSArray*, NSError*)) block;
-(void) getStatesToCreateAddressWithBlock:(void (^) (NSArray*, NSError*)) block;
-(void) choiceShippingAddress: (Address*) address  withBlock:(void (^) (BOOL, NSError*)) block;

#pragma Cart
-(void) getCartWithBlock:(void (^) (NSObject*, NSError*)) block;
-(void) insertProductsOnCart:(NSArray*) products withBlock:(void (^) (BOOL, NSError*)) block;
-(void) updateProductsOnCart:(NSArray*) products withBlock:(void (^) (BOOL, NSError*)) block;
-(void) deleteProductOnCart:(Product*) product withBlock:(void (^) (BOOL, NSError*)) block;
-(void) confirmCheckoutWithCart:(Cart*) cart withBlock:(void (^) (NSObject*, NSError*)) block;


#pragma Payments
-(void) getPaymentMethodsWithBlock:(void (^) (NSArray*, NSError*)) block;
-(void) insertPaymentMethod:(PaymentMethod*) paymentMethod WithBlock:(void (^) (BOOL, NSError*)) block;

#pragma Shipping
-(void) getShippingMethodsWithBlock:(void (^) (NSArray*, NSError*)) block;
-(void) insertShippingMethod:(ShippingMethod*) paymentMethod WithBlock:(void (^) (BOOL, NSError*)) block;

#pragma Order
-(void) finnishOrderWithBlock:(void (^) (BOOL, NSString* , NSError*)) block;

@end
