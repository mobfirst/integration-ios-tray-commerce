//
//  Constants.h
//  PlatformModels
//
//  Created by Diego P Navarro on 15/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#ifndef PlatformModels_Constants_h
#define PlatformModels_Constants_h

typedef enum {
    BUSINESS,
    ENTERPRISE
} AppModel;

#endif
