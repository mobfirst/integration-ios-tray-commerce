//
//  Store.h
//  PlatformModels
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Address.h"
#import "Constants.h"

@interface Store : NSObject

@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * logo;
@property (nonatomic, strong) NSString * url;

@property (nonatomic, strong) NSString * facebook;
@property (nonatomic, strong) NSString * instagram;
@property (nonatomic, strong) NSString * instagramTag;
@property (nonatomic, strong) NSString * twitter;
@property (nonatomic, strong) NSString * youtube;

@property (nonatomic, strong) NSString * color;
@property (nonatomic) BOOL active;
@property (nonatomic) BOOL debug;
@property (nonatomic) AppModel model;

@property (nonatomic, strong) NSArray * phones;
@property (nonatomic, strong) NSArray * emails;
@property (nonatomic, strong) NSArray * addresses;
@property (nonatomic, strong) NSArray * banners;

@end
