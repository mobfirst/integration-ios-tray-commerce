//
//  Email.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 02/12/14.
//  Copyright (c) 2014 Vitrina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Email : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * email;

@end
