//
//  Variation.h
//  PlatformModels
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Variation : NSObject

@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * value;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * specialPrice;

@property (nonatomic, strong) NSArray * variations;

@end
