//
//  ProductCartToUpdate.h
//  PlatformModels
//
//  Created by Diego P Navarro on 23/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductCart.h"

@interface ProductCartToUpdate : NSObject

@property(strong,nonatomic) NSString* id;
@property(strong,nonatomic) ProductCart* product;
@property(nonatomic) int newQuantity;

@end
