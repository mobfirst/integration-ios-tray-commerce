//
//  Product.h
//  PlatformModels
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Option.h"

@interface Product : NSObject

@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * specialPrice;
@property (nonatomic, strong) NSString * productDescription;

@property (nonatomic, strong) Option * option;
@property (nonatomic, strong) NSMutableArray * images;

@end
