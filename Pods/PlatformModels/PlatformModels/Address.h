//
//  Address.h
//  PlatformModels
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Address : NSObject

@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * addressLine1;
@property (nonatomic, strong) NSString * addressLine2;
@property (nonatomic, strong) NSString * number;
@property (nonatomic, strong) NSString * neighborhood;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * state;
@property (nonatomic, strong) NSString * country;
@property (nonatomic, strong) NSString * postalCode;

@end
