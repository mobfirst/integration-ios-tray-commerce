//
//  ServiceStorageStore.h
//  PlatformModels
//
//  Created by Diego P Navarro on 15/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Store.h"

@interface ServiceStorageStore : NSObject

+(BOOL) saveDefaultStore:(Store*) store;
+(Store*) getDefaultStore;

@end
