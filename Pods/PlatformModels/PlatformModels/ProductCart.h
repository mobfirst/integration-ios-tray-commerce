//
//  ProductCart.h
//  PlatformModels
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@interface ProductCart : Product

@property (nonatomic) NSInteger quantity;

@end
