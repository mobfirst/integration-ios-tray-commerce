//
//  ProductCategory.h
//  PlatformModels
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductCategory : NSObject

@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSMutableArray * childrens;

@end
