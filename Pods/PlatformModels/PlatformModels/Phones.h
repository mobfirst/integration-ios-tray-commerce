//
//  Phones.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 02/12/14.
//  Copyright (c) 2014 Vitrina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Phones : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * areaCode;
@property (nonatomic, strong) NSString * number;
@property (nonatomic, strong) NSString * countryCode;

@end
