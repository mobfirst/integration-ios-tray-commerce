//
//  PaymentMethod.h
//  PlatformModels
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentMethod : NSObject

@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * name;

@end
