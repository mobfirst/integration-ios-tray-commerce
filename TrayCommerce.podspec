Pod::Spec.new do |s|  
  s.name         = "TrayCommerce"
  s.version      = "0.3.1"
  s.summary      = "A short description of TrayCommerce."

  s.description  = <<-DESC
                   A longer description of TrayCommerce in Markdown format.
                   DESC

  s.homepage     = "http://EXAMPLE/TrayCommerce"
  s.license      = 'MIT'
  s.author       = { "Diego Palma" => "diego@vitrina.cc" }
  s.platform     = :ios, '7.0'
  s.source_files  = 'TrayCommerce', 'TrayCommerce/**/*.{h,m}'
  s.public_header_files = 'TrayCommerce/**/*.h'
  s.resources    = "TrayCommerce/*.png"
  s.framework    = 'SystemConfiguration'
  s.requires_arc = true
  s.source       = {
    :git => 'git@bitbucket.org:mobfirst/integration-ios-tray-commerce.git'
  }

end 
