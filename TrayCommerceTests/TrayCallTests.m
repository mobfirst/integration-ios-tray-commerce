//
//  TrayCallTests.m
//  TrayCommerce
//
//  Created by Diego P Navarro on 21/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ServiceApiMapper.h"

@interface TrayCallTests : XCTestCase

@end

@implementation TrayCallTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//Blocker the Main Thread
static inline void mainLoopBlocker(void(^block)(BOOL *done)) {
    __block BOOL done = NO;
    block(&done);
    while (!done) {
        [[NSRunLoop mainRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:.1]];
    }
}

- (void)testCallGetInfo {
 
    mainLoopBlocker(^(BOOL *done) {
    
        [[ServiceApiMapper Api] getStoreInformationWithBlock:^(NSObject *object, NSError * error) {
            
            XCTAssertNotNil(object, @"Object is Null");
            
            if(error){
                XCTAssertNil(error, @"Error isn't Null");
                *done = YES;
            }else{
                [[ServiceApiMapper Api] getCategoriesWithBlock:^(NSArray * objects , NSError * error) {
                   
                    if(error){
                        XCTAssertNotNil(objects, @"Object is Null");
                        *done = YES;
                    }else{
                    
                        [[ServiceApiMapper Api] getAllProductsOnPage:1 andQuantity:10 withBlock:^(NSArray * object , NSError * error) {
                        
                            XCTAssertNotNil(object, @"List is Null");
                        
                            *done = YES;
                        }];
                    }
                }];
            }
        }];
    });
}

-(void) testGetCategories{
    
    mainLoopBlocker(^(BOOL *done) {
        
        [[ServiceApiMapper Api] getCategoriesWithBlock:^(NSArray * object , NSError * error) {
        
            XCTAssertNotNil(object, @"Object is Null");
            XCTAssertTrue([object count] == 4, @"Array size isn't 4");
            ProductCategory* cat = [object objectAtIndex:0];
            XCTAssertTrue([cat.id isEqualToString:@"2"], @"First id isn't 2");
            XCTAssertTrue([cat.name isEqualToString:@"Futebol"], @"First name isn't Futebol");
            XCTAssertTrue([cat.childrens count] == 1, @"First Childrens count isn't 1");
            
            ProductCategory* catChild = [cat.childrens objectAtIndex:0];
            XCTAssertTrue([catChild.id isEqualToString:@"4"], @"First Child id isn't 4");
            XCTAssertTrue([catChild.name isEqualToString:@"Bolas"], @"First Child id isn't Bolas");
            XCTAssertNil(catChild.childrens , @"First Childrens error");
            
            *done = YES;
            
        }];
    });
}

-(void) testGetAllProducts{
    
    mainLoopBlocker(^(BOOL *done) {
        
        [[ServiceApiMapper Api] getAllProductsOnPage:1 andQuantity:1 withBlock:^(NSArray * object , NSError * error) {
            
            XCTAssertNotNil(object, @"List is Null");
            XCTAssertTrue([object count] == 1, @"Array size isn't 1");
            
            Product* product = [object objectAtIndex:0];
            XCTAssertTrue([product.id isEqualToString:@"2"], @"Product id isn't 2");
            XCTAssertTrue([product.name isEqualToString:@"Bola Adidas Cafusa"], @"Product name isn't Bola Adidas Cafusa");
            XCTAssertTrue([product.price isEqualToString:@"49.90"], @"Product price isn't 49.90");
            
            XCTAssertTrue([product.images count] == 1, @"Array size isn't 1");
            
            *done = YES;
        }];
    });
}

-(void) testGetProductsByCategoryId{
    
    mainLoopBlocker(^(BOOL *done) {
        
        [[ServiceApiMapper Api] getProductsByCategoryId:@"2" onPage:1 andQuantity:10 withBlock:^(NSArray * object, NSError * error) {

            XCTAssertNotNil(object, @"List is Null");
            XCTAssertTrue([object count] == 1, @"Array size isn't 1");
            
            Product* product = [object objectAtIndex:0];
            XCTAssertTrue([product.id isEqualToString:@"2"], @"Product id isn't 2");
            XCTAssertTrue([product.name isEqualToString:@"Bola Adidas Cafusa"], @"Product name isn't Bola Adidas Cafusa");
            XCTAssertTrue([product.price isEqualToString:@"49.90"], @"Product price isn't 49.90");
            
            XCTAssertTrue([product.images count] == 1, @"Array size isn't 1");
            
            *done = YES;
            
        }];
    });
    
}

-(void) testGetProductsBySearch{
    
    mainLoopBlocker(^(BOOL *done) {
        
        [[ServiceApiMapper Api] getProductsBySearch:@"bola" onPage:1 andQuantity:10 withBlock:^(NSArray * object, NSError * error) {
            
            XCTAssertNotNil(object, @"List is Null");
            XCTAssertTrue([object count] == 2, @"Array size isn't 2");
            
            Product* product = [object objectAtIndex:0];
            XCTAssertTrue([product.id isEqualToString:@"2"], @"Product id isn't 2");
            XCTAssertTrue([product.name isEqualToString:@"Bola Adidas Cafusa"], @"Product name isn't Bola Adidas Cafusa");
            XCTAssertTrue([product.price isEqualToString:@"49.90"], @"Product price isn't 49.90");
            
            XCTAssertTrue([product.images count] == 1, @"Array size isn't 1");
            
            *done = YES;
            
        }];
    });
}

-(void) testGetProductById{
    
    mainLoopBlocker(^(BOOL *done) {
        [[ServiceApiMapper Api] getProductById:@"12" withBlock:^(NSObject * object, NSError * error) {
            
            XCTAssertNotNil(object, @"Object is Null");
            
            Product* product = object;
            
            XCTAssertTrue([product.id isEqualToString:@"12"], @"Product Id isn't 12");
            XCTAssertTrue([product.name isEqualToString:@"Agasalho São Paulo"], @"Product Name isn't Tênis Reebok U-Top X");
            XCTAssertTrue([product.specialPrice isEqualToString:@"89.90"], @"Product Special Price isn't 89.90");
            XCTAssertTrue([product.price isEqualToString:@"139.90"], @"Product Price isn't 139.90");
            
            XCTAssertTrue([product.images count] == 4, @"Product Images isn't 4");
            
            NSString* image = [product.images objectAtIndex:0];
            XCTAssertTrue([image isEqualToString:@"https://images.tcdn.com.br/img/img_prod/1/12_1_20131120102019.jpg"], @"Product Image Error");
            
            XCTAssertNotNil(product.option.childrens,@"Variation is null");
            
            Variation* variation = [product.option.childrens objectAtIndex:0];
            
            XCTAssertTrue([variation.id isEqualToString:@"12"], @"Variation Id isn't 12");
            
            XCTAssertTrue([variation.price isEqualToString:@"89.90"], @"Variation Price isn't 89.90");
           
            XCTAssertTrue([variation.value isEqualToString:@"Cor : Preto - Tamanho : P "], @"Variation Price isn't Cor : Preto - Tamanho : P ");
            
            
            *done = YES;
        }];
    });
}

@end
